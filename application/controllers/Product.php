<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Product extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "pdtb_product";

	}
	public function getlist()
	{

		$sql = "SELECT t1.id, t1.name, t1.code, t1.images, t1.pin, t1.hot,
		
		t1.page_id, t1.price_sale, t1.price, t1.orders, t1.views, 
		
		t1.percent, t1.status, t1.maker_date, t2.name AS namegroup		
		
		FROM " . $this->table . " AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id AND t2.id !=0";
	
		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}
	
	public function getrow()
	{
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$sql = "SELECT * FROM " . $this->table . " where id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();
		
		//$list->attribute = $this->listAttribute($id);

		//$list->priceattr = $this->listPriceAttribute($id);

		$this->responsesuccess(null, $list);
	}
	
	public function getdocument () {
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$sql = "SELECT t1.id AS document_product_id, t2.id, t2.name, t2.keywords
		
		FROM tb_document_product AS t1

		LEFT JOIN tb_config_product_document AS t2 ON t1.document_id = t2.id

		WHERE t1.product_id = " . $id;

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}
	
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$data['code'] = time();
			
			$data['status'] == true ?  1 : 0;

			$data['link'] = (array_key_exists('link', $data) && strlen($data['link']) > 1) ? removesign($data['link']) : removesign($data['name']);

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE link='" . $data['link'] . "'";

			if ($id > 0) {

				$sql .= " and id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

					$id = $this->db->insert_id();
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('checkExitname');
			}
		}


		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}

	
	public function  updateImages()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('images', $data)) {

				$data['images'] = $this->processimages($data['images']);
			}else {
				
				$data['images'] ='';
			}
			
			
			if (array_key_exists('listimages', $data)) {

				$data['listimages'] = $this->processimages($data['listimages']);
				
			}else {
				
				$data['listimages'] ='';
			}

			if ($id > 0) {

				$this->db->where('id', $id);

				$is = $this->db->update($this->table, $data);
			}
		}

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function updateDocument () {
		
		$table_document_product = 'tb_document_product';
		
		$table_document_customer = 'tb_document_customer';
				
		$data = $this->getdata();
		
		$dataAdd = $data['add'];

		$dataRemove = $data['remove'];
		
		$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;
				
		$is = count($dataAdd) > 0 || count($dataRemove) > 0 ? false : true;
		
		for($i = 0; $i < count($dataAdd); $i ++){
			
			$id = $dataAdd[$i]['id'];
				
			unset($dataAdd[$i]['sync']);
			
			if($id == 0){
				
				$dataAdd[$i]['create_user'] = $this->session->userdata('user_id');
				
				$dataAdd[$i]['create_date'] = date('Y-m-d H:i:s');
				
				$is = $this->db->insert($table_document_product, $dataAdd[$i]);
			
			} else {
				unset($dataAdd[$i]['id']);
				$dataAdd[$i]['maker_id'] = $this->session->userdata('user_id');
				$dataAdd[$i]['maker_date'] = date('Y-m-d H:i:s');
				$this->db->where('id', $id);
				$is = $this->db->update($table_document_product, $dataAdd[$i]);
			}
		}
		
		for($i = 0; $i < count($dataRemove); $i++){
			$this->db->where('id', $dataRemove[$i]['document_product_id']);
			$is = $this->db->delete($table_document_product);
		}
		
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}
	
	public function syncDocument (){
		
		$table_document_customer = 'tb_document_customer';
		
		$type = $this->params['type'];
		
		$product_id = $this->params['product_id'];
		
		$document_id = $this->params['document_id'];
		
		$is = false;
		
		if($type == 'add'){
			// add to order
			$sql = "SELECT id
			FROM ortb_order_detail
			WHERE product_id = ".$product_id." AND id NOT IN (
			SELECT order_detail_id 
			FROM tb_document_customer
			WHERE document_product_id = ".$document_id.")";
			
			$query = $this->db->query($sql);

			$listAdd = $query->result_object();
			
			$is = count($listAdd) > 0 ? false : true;
			
			for($j = 0; $j < count($listAdd); $j ++){
				$item['document_product_id'] = $document_id;
				$item['order_detail_id'] = $listAdd[$j]->id;
				$item['create_user'] = $this->session->userdata('user_id');
				$item['create_date'] = date('Y-m-d H:i:s');
				$item['status'] = 1;
				$is = $this->db->insert($table_document_customer, $item);
			}
			
		} else {
			
			// remove to order
			$sql = "SELECT t2.id
			FROM ortb_order_detail AS t1
			LEFT JOIN tb_document_customer AS t2 ON t1.id = t2.order_detail_id
			WHERE t1.product_id = ".$product_id." AND t2.document_product_id = ".$document_id;
					
			$query = $this->db->query($sql);
			
			$listRemove = $query->result_object();
			
			if(count($listRemove) > 0){
				$listRemove = array_keys(array_reduce($listRemove, function ($n, $o) {
					$n[$o->id] = $o;
					return $n;
				}));
				
				$this->db->where_in('id', $listRemove);
				$is =  $this->db->delete($table_document_customer);
			} else {
				$is = true;
			}
		}
		
		$message = ($type == 'add') ? 'syncAddSuccess' : 'syncRemoveSuccess';
		
		$is == true ? $this->responsesuccess($this->lang->line($message)): $this->responsefailure($this->lang->line('failure'));
	}

	public function updateSeo(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$data = $this->getdata();
				
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('description' => $data['description'],'keywords' => $data['keywords']));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));

	}

	public function updateVideos(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$data = $this->getdata();
				
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('videos' => $data['videos']));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));

	}

	public function remove(){
		
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');
		
		$sql = "SELECT COUNT(id) as count FROM tb_class WHERE page_id = " . $id;

		if ($this->db->query($sql)->row_object()->count == 0) {

			$sql="select * from ".$this->table." where id=".$id;
			
			$list = $this->db->query($sql)->row_object();
									
			$this->db->where('id', $id);
			
			$is = $this->db->delete($this->table);
			
			if(isset($list->images) && strlen($list->images) > 4){
				
				@unlink('public/products/'.$list->images);
			}
			if(isset($list->banner) && strlen($list->banner) > 4){
				
				@unlink('public/products/'.$list->banner);
			}
			if(isset($list->images_specification) && strlen($list->images_specification) > 4){
				
				@unlink('public/specification/'.$list->images_specification);
			}
			if(isset($list->file_specification) && strlen(file_specification) > 4){
				
				@unlink('public/file/'.$list->file_specification);
			}
			if(isset($list->listimages) && strlen($list->listimages) > 4){
				
				$listimages	= json_decode($list->listimages);	
						
				if (count($listimages) > 0) {
			
					foreach ($listimages as $k) {
												
						@unlink('public/products/'.$k);
					}
				}
			}
			
			$message = ($is==true) ? $this->lang->line('success') : $this->lang->line('failure');
			
		} else {

			$message .= $this->lang->line('checkDeleteProduct');
		}
		
		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
		
	}
    
	public function listAttribute($id)
	{
		
		$sql = " SELECT t1.id, t1.name, t2.value, t2.attribute_id FROM pdtm_attribute AS t1
		
		LEFT JOIN pdtb_product_att AS t2 ON t1.id = t2.attribute_id		
		
		WHERE t1.status =1 AND t2.product_id = ".$id;
		
		$list = $this->db->query($sql)->result_object();
		
		return $list;
	}
	
	public function updateAttribute()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$is = false;
		
		if (count($data) > 0 && $id > 0) {
			
			for($i = 0; $i < count($data); $i++){
			    
				$data[$i]['maker_id'] = $this->session->userdata('user_id');
				
				$data[$i]['maker_date'] = date('Y-m-d H:i:s');
				
				unset($data[$i]['name']);
				
				unset($data[$i]['list']);
				
				$where = " product_id =" .$data[$i]['product_id'] . "  AND attribute_id =" . $data[$i]['attribute_id'] . "";
				
				$sql = "select count(id) as count from pdtb_product_att where ". $where;
				
				if ($this->db->query($sql)->row_object()->count == 0) {
						
					$this->db->insert('pdtb_product_att', $data[$i]);
				
				}else{
					
					$this->db->where($where);
					
					$this->db->update('pdtb_product_att', $data[$i]);
				}	
				
			}
			
			$is = true;	
		}
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}

	public function changePin(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('pin' => $pin));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	public function changeHot(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$hot = isset($this->params['hot']) ? $this->params['hot'] : 0;
		
		$maker_date = date('Y-m-d H:i:s');
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('hot' => $hot,'maker_date' => $maker_date));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	/**
     * 
     * Create
     * Name: LÊ CHIẾNTRUC 
     * Date : 23/01/2021
     * Note: UPDATE DEVICE, UPDATE PRICE
     * ------------------
     * Edit
     * Name:  NGUYEN VAN TIEN
     * Date : 07/06/2021
     * Note : UPDATE PRICE ATTRIBUTE
     *
     **/
	public function updateDevice(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$data = $this->getdata();
				
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('device' => $data['device']));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}
	
	public function updatePrice(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$data = $this->getdata();
				
		$is = false;
		
		if(array_key_exists('price_sale' , $data) && $data['price_sale']>0 && $data['price_sale']!=null){			
			$data['percent']=100 - round(((($data['price_sale']*100)/$data['price'])));
		} else { 
			$data['percent']=0;
		}
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, $data);
		
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}
	public function listPriceAttribute($id)
	{
		
		$sql = " SELECT t1.id, t1.product_id, t1.price, t1.att_id_value, t1.images , t1.status  FROM pdtb_product_price_att AS t1
				
		WHERE t1.status = 1 AND t1.product_id = ".$id." ORDER BY t1.id ASC";
		
		$list = $this->db->query($sql)->result_object();
		
		return $list;
	}
	public function updatePriceAtribute(){
		
		$product_id = isset($this->params['product_id']) ? (int) $this->params['product_id'] : 0;
		
		$data = $this->getdata();
		
		$is = false;
		
		if (count($data) > 0 && $product_id > 0) {

			for($i = 0; $i < count($data); $i++){

				$data[$i]['maker_id'] = $this->session->userdata('user_id');

				$data[$i]['maker_date'] = date('Y-m-d H:i:s');

				$data[$i]['product_id'] = $product_id;

				if (array_key_exists('images', $data[$i])) {

					$data[$i]['images'] = $this->processimages($data[$i]['images']);
					
				}else {
					
					$data[$i]['images'] ='';
				}

				$data[$i]['status'] = 1;
				
				$id = array_key_exists('id' , $data[$i])  > 0 ? (int)$data[$i]['id'] : 0;
			
				if ($id > 0) {

					$this->db->where('id', $id);
					
					$is = $this->db->update('pdtb_product_price_att', $data[$i]);
					
				}else{
					
					$is = $this->db->insert('pdtb_product_price_att', $data[$i]);
				}	
				
			}
			
			$is = true;	
		}
		
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
	}

	public function removePriceAtribute(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;
		
		$this->db->where('id', $id);
		
		$is = $this->db->delete('pdtb_product_price_att');
		
		$is == true ? $this->responsesuccess($this->lang->line('success')): $this->responsefailure($this->lang->line('failure'));
		
	}
	/** ================== END UPDATE DEVICE ==================**/


	public function changeStatus() {
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$status = isset($this->params['status']) ? $this->params['status'] : 0;
		
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('status' => $status));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
	}
	/* ================== END CHANGE STATUS ================== */


}
