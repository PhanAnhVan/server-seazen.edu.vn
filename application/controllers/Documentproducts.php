<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentproducts extends MY_Controller {
	
	public function __construct(){
		
		parent:: __construct();

		$this->table = "tb_config_product_document";
		
	}
	public function getlist(){	
	
		$sql="SELECT id, name, description, keywords, status, create_date FROM " . $this->table;
		
		$sql .= " ORDER BY create_date DESC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function getrow(){

		$id = $this->params['id'];

		$sql="SELECT id, code, name, description, detail, keywords, status, maker_date FROM ".$this->table." WHERE id=".$id;

		$query=$this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function process(){
		
		$data = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');
		
		if ($data !== null) {
			
			$id = $id >0 ? $id : (array_key_exists('id',$data) ? (int) $data['id'] : 0);

            if($id == 0){
				
				$data['code'] = time();
				
				$data['create_user'] = $this->session->userdata('user_id');
				
				$data['create_date'] = date('Y-m-d H:i:s');
				
            } else {
				
				$data['maker_id'] = $this->session->userdata('user_id');
				
				$data['maker_date'] = date('Y-m-d H:i:s');
			}
          		
			$sql ="SELECT COUNT(id) AS count FROM ".$this->table." WHERE name='".$data['name']."'";
	
			if ($id > 0) {
				
				$sql .= " AND id!=" . $id;
			}
							
			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);
					
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}
			}

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
		}
		
		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{
		$id = $this->params['id'];

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "SELECT COUNT(id) as count FROM tb_document_product WHERE document_id =" . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);
			} else {

				$message .= $this->lang->line('checkDeleteDocument');
			}
		}

		($is) ? $this->responsesuccess($message) : $this->responsefailure($message);
	}
}
 
?>