<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apiquettin extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	public function processApi(){
	    
	    $path =  './public/contents/';
		
		$list = $this->getdata();
		
		$data = $list['data'];
		
		$token = $list['token'];
		
		$is = false;

		$message = $this->lang->line('failure');
		
		
	    if(!empty($data)){
	        
	        $sql ="SELECT COUNT(id) AS count FROM wstm_setting WHERE value='".$token."'";
	        
	        if ($this->db->query($sql)->row_object()->count == 1) {
	            
	            $countContentExit = 0;
	        
    	        $countContentScan = 0;
    	        
    	        $countContent =count($data);
	         
    	        for($i = 0 ; $i < count($data); $i++){
                
                    $data[$i]['maker_id'] = 1;
                    
                    $data[$i]['status'] = 1;
                    
                    $data[$i]['maker_date'] = date('Y-m-d H:i:s');
                    
                    $data[$i]['page_id'] = $data[$i]['group_id'] ;
                    
                    unset($data[$i]['group_id']);
                    unset($data[$i]['images_seo']);
                  
                    $data[$i]['images'] = $this->processImagesApi($data[$i]['images'], $path, $i);
                   
            		$data[$i]['link']= (array_key_exists('link' , $data[$i]) && strlen($data[$i]['link']) > 0) ? removesign($data[$i]['link']) : removesign($data[$i]['name']);
            	 
            		$sql ="SELECT COUNT(id) AS count FROM wstm_content WHERE link='".$data[$i]['link']."'";
             
            		if ($this->db->query($sql)->row_object()->count == 0) {
            
            		     $is = $this->db->insert('wstm_content', $data[$i]);
        		    
        		        $countContentScan = $is == true ?  $countContentScan + 1: $countContentScan;
            		    
            		}else{
            		    
            		    $countContentExit = $countContentExit + 1;
            		    
            		    continue;
            		   
            		}
                }
                
                if($countContent == $countContentExit){
                
                    $message =  'Tất cả tin đã được nạp trước đó';
                    
                }else if($countContent == $countContentScan){
                    
                    $message =  'Tất cả tin đã được nạp vào thành công';
                    
                }else {
                    
                    $message =  "Đã nạp được ".$countContentScan." tin và có ".$countContentExit." đã được nạp trước đó";;
                }
    	    } else {
	        
    	        $message = "Token bị sai";
    	    }
	    }else{
	        
	        $message = 'Không tồn tại dữ liệu';
	    }
		
		if ($is == true) {

			$this->responsesuccess($message,$data);
			
		} else {
			$this->responsefailure($message,$data);
		}
	}
	
	function processImagesApi($image, $path ,$i) {
	    
	    $extension = substr(explode( '/', $image )[1],0, 3);
	    
	    $newname = $i .'-' . time() . '.' . $extension;
	    
	    if(is_array(getimagesize($image))){
	        
	        @$file = file_get_contents($image);

			@$result = file_put_contents($path . $newname, $file);

	    }
	    
	   return $newname;
	 
    }
}