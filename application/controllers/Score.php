<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Score extends MY_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->table = "tb_class";
	}
  
  	public function getClass(){
      	
      	$type = isset($this->params['type']) ? $this->params['type'] : 0;
		
		$teacher_id = isset($this->params['teacher_id']) ? $this->params['teacher_id'] : 0;
		
		$sql = "SELECT t1.id,  t1.page_id, t1.name
		
		FROM ".$this->table." AS t1 
		
		LEFT JOIN tb_class_product AS t2 ON t1.id = t2.class_id 
        
        WHERE t1.status = 1 ";
        
        if($type == 1){
          	
          	$sql .=" AND t2.teacher_id =".$teacher_id;
          
        }
      
        $sql .=" GROUP BY t1.id ORDER BY t1.id ASC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function getCourseClass(){

		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
		
		$teacher_id = isset($this->params['teacher_id']) ? $this->params['teacher_id'] : 0;
		
		$sql=" SELECT t1.product_id, t2.name , 

		(CASE WHEN t3.total_pass IS null THEN 0 ELSE t3.total_pass END) AS total_pass,

		(CASE WHEN t4.total_nopass IS null THEN 0 ELSE t4.total_nopass END) AS total_nopass,

		(CASE WHEN t6.total_noscore IS null THEN 0 ELSE t6.total_noscore END) AS total_noscore,

		t5.total_student, t5.status as status_end, t5.is_lock
		
		FROM tb_class_product AS t1 
		
		LEFT JOIN pdtb_product AS t2 ON t1.product_id = t2.id
        
        LEFT JOIN (SELECT count(id) as total_pass, class_id, product_id 
        			
                   FROM tb_student_product 
                   
                   WHERE class_id = ". $class_id ." AND is_pass = 1 GROUP BY product_id) AS t3 ON t1.class_id = t3.class_id AND t1.product_id = t3.product_id
		
		LEFT JOIN (SELECT count(id) as total_nopass, class_id, product_id 
        
        		   FROM tb_student_product 
                    
                   WHERE class_id=  ". $class_id ." AND is_pass = 0 GROUP BY product_id) AS t4 ON t1.class_id = t4.class_id AND t1.product_id = t4.product_id

       LEFT JOIN (SELECT count(id) as total_noscore, class_id, product_id 
        
        		   FROM tb_student_product 
                    
                   WHERE class_id=  ". $class_id ." AND is_pass IS null GROUP BY product_id) AS t6 ON t1.class_id = t6.class_id AND t1.product_id = t6.product_id
                   
        LEFT JOIN (SELECT count(id) as total_student, is_lock, status, class_id, product_id 
        
        		   FROM tb_student_product 
                    
                   WHERE class_id =  ". $class_id ." GROUP BY product_id) AS t5 ON t1.class_id = t5.class_id AND t1.product_id = t5.product_id
        
        WHERE t1.class_id = ". $class_id;

		if($teacher_id> 0){
		
			$sql .=" AND t1.teacher_id = ". $teacher_id;

		}

		$sql .=" ORDER BY t1.product_id ASC";

		$query = $this->db->query($sql);

		$data = $query->result_object();
      
		$this->responsesuccess($this->lang->line('success') , $data);
	
	}
	
	public function getListStudent(){
		
		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
		
		$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;
		
		$sql=" SELECT t1.id, t1.score, t1.score_pass, t1.note,t1.student_id, t1.is_pass, t1.status, 

		t1.is_attendance, t1.score_factor_1, t1.score_factor_2, t1.is_discipline, t1.score_1, t1.score_2, t1.score_avg, t2.name
		
		FROM tb_student_product AS t1 
		
		LEFT JOIN tb_student AS t2 ON t1.student_id = t2.id
		
		WHERE  t1.class_id = ". $class_id ." AND t1.product_id =".$product_id." GROUP BY student_id ORDER BY t1.maker_date ASC";
		
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function process()
	{
		$data = $this->getdata();
		
		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			for($i = 0 ; $i < count($data); $i++){
				
				$id = $data[$i]['id'];
				
				unset($data[$i]['name']);
				
				unset($data[$i]['id']);
		
				$this->db->where('id', $id);
				
				$is = $this->db->update('tb_student_product', $data[$i]);
			}

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
		}

		$is == true ? 	$this->responsesuccess($message) :	$this->responsefailure($message);
	}
  
  
  	public function lockCourse()
	{
		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
		
		$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;
      
      	$data = $this->getdata();

		$message = $this->lang->line('failure');
      
      	$is = false;

      	if($data >= 0){
          
          	$arrayData =  array('is_lock' => $data);
    
            $arrayWhere = array('class_id' => $class_id, 'product_id' => $product_id);

            $this->db->set($arrayData);

            $this->db->where($arrayWhere);
          
          	$is = $this->db->update('tb_student_product');
          
        }

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
            
		$is == true ? 	$this->responsesuccess($message) :	$this->responsefailure($message);
	}
  
  	public function endCourse()
	{
		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
		
		$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;
      
      	$data = $this->getdata();

		$message = $this->lang->line('failure');
      
      	$is = false;
 
        $arrayData =  array('accept_score_id' => $this->session->userdata('user_id'), 'accept_score_time' => date('Y-m-d H:i:s'), 'is_lock' => 1,'status' => 2);

        $arrayWhere = array('class_id' => $class_id, 'product_id' => $product_id);

        $this->db->set($arrayData);

        $this->db->where($arrayWhere);

        $is = $this->db->update('tb_student_product');
      
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
            
		$is == true ? 	$this->responsesuccess($message) :	$this->responsefailure($message);
	}
  
	public function calculateAVG(){
        
    	$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;

      	$page_id = isset($this->params['page_id']) ? $this->params['page_id'] : 0;
      
      	$listProduct =$this->getdata() ;
      
      	$is = false;

      	$message = $this->lang->line('failure');
      
      	$sql = "SELECT student_id, class_id, ROUND(AVG(score_avg),2) as score_avg, MIN(is_pass) as is_pass, 0 AS page_id, '' as list_product

                FROM `tb_student_product` 

                WHERE class_id = ".$class_id." GROUP BY student_id";

      	$query = $this->db->query($sql);

      	$data = $query->result_object();

      	if ($listProduct !== null) {
          
          	$sql = "SELECT count(id) as count FROM tb_student_majors WHERE class_id = ".$class_id;
          
          	if($this->db->query($sql)->row_object()->count == 0){

                for($i = 0 ; $i< count($data); $i++){

                    $data[$i]->page_id = $page_id;

                    $data[$i]->maker_id = $this->session->userdata('user_id');

                    $data[$i]->maker_date = date('Y-m-d H:i:s');

                    $data[$i]->status =  $data[$i]->is_pass == 1 ? 1 : 0;

                    $data[$i]->list_product = json_encode($listProduct);

                    $is = $this->db->insert('tb_student_majors', $data[$i]);

                }
              
            } else {
              
                for($i = 0 ; $i< count($data); $i++){

                 	$arrayData =  array('is_pass' => $data[$i]->is_pass, 'score_avg' => $data[$i]->score_avg, 'maker_id' => $this->session->userdata('user_id'), 'maker_date' => date('Y-m-d H:i:s'), 'status' => $data[$i]->is_pass == 1 ? 1 : 0);
                 
                 	$arrayWhere = array('class_id' => $data[$i]->class_id, 'student_id' => $data[$i]->student_id);
                 	
                 	$this->db->set($arrayData);
        
                    $this->db->where($arrayWhere);

                    $is = $this->db->update('tb_student_majors');

                }
            }
        }

      	$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

      	$is == true ? 	$this->responsesuccess($message) :	$this->responsefailure($message);
    }
}
?>