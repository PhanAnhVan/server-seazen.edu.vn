<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classcourse extends MY_Controller {
	
	public function __construct(){
		parent:: __construct();
		$this->table = "tb_class";
	}

	public function getlist(){
		
		$sql="SELECT t1.id,t1.code, t1.page_id, t1.name, t1.total_student, t1.status, t1.is_lock,  t1.maker_date, t2.name AS majors 
		
		FROM ".$this->table." AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id  ORDER BY t1.id ASC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function getrow(){

		$id = $this->params['id'];

		$sql="SELECT * FROM ".$this->table." WHERE id=".$id;

		$query=$this->db->query($sql);

		$data = $query->row_object();
		
		$sql=" SELECT t1.product_id,  t1.teacher_id, t2.name, t3.name as name_teacher
		
		FROM tb_class_product AS t1 
		
		LEFT JOIN pdtb_product AS t2 ON t1.product_id = t2.id 
        
        LEFT JOIN hrtb_user AS t3 ON t1.teacher_id = t3.id 
		
		WHERE t1.class_id =". $id;
	
		$query = $this->db->query($sql);

		$data->course = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $data);
	}

	

	public function process()
	{
		$item = $this->getdata();

		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = 0;

		$message = $this->lang->line('failure');

		if ($item !== null) {

			$id = $id > 0 ? $id : (array_key_exists('id',$item['data']) ? (int) $item['data']['id'] : 0);

			$item['data']['maker_id'] = $this->session->userdata('user_id');
			
			$item['data']['maker_date'] =  date('Y-m-d H:i:s');

			$sql ="SELECT COUNT(id) AS count FROM ".$this->table." WHERE (code='".$item['data']['code']."' OR name ='".$item['data']['name']."' )";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;

			}
            
			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {
					
					$is = $this->db->insert($this->table, $item['data']);
					
					$class_id = $this->db->insert_id();
					
					if($is){
						
						for($i = 0 ; $i < count($item['dataClassProduct']); $i++){
							
							unset($item['dataClassProduct'][$i]['name']);
                          
                          	unset($item['dataClassProduct'][$i]['name_teacher']);
                            
                            unset($item['dataClassProduct'][$i]['flags']);
							
							$item['dataClassProduct'][$i]['class_id'] = $class_id;
							
							$item['dataClassProduct'][$i]['maker_id'] = $this->session->userdata('user_id');
							
							$item['dataClassProduct'][$i]['maker_date'] = date('Y-m-d H:i:s');
							
							$item['dataClassProduct'][$i]['status'] = 1;
							
							$is = $this->db->insert('tb_class_product', $item['dataClassProduct'][$i]);

						}
					}

				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $item['data']);
					
					$class_id = $id;
						
					if($is){
						
						for($i = 0 ; $i < count($item['dataClassProduct']); $i++){
							
							unset($item['dataClassProduct'][$i]['name']);
                          
                          	unset($item['dataClassProduct'][$i]['name_teacher']);
                            
                            unset($item['dataClassProduct'][$i]['flags']);
							
							$this->db->where('class_id', $class_id);
							
							$this->db->where('product_id',$item['dataClassProduct'][$i]['product_id']);
							
							$is = $this->db->update('tb_class_product', $item['dataClassProduct'][$i]);
						}
					}

				}
			} else {
				
             	$is = -1; 
            }

			$message = ($is == 1) ? $this->lang->line('success') : ($is == -1 ? $this->lang->line('exitCodeClass') : $this->lang->line('failure'));
		}

		$is == 1 ? 	$this->responsesuccess($message, $class_id) :	$this->responsefailure($message);
	}

	public function getCourse(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$sql=" SELECT t1.id as product_id, t1.name, 0 as teacher_id
		
		FROM pdtb_product AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id 
		
		WHERE t2.type = 3 AND t2.id =" . $id ." AND t1.status = 1 ORDER BY t1.maker_date ASC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);

	}
	
	public function getTeacher(){

		$sql=" SELECT id, name
		
		FROM hrtb_user 
		
		WHERE type = 1 AND status = 1 ORDER BY maker_date ASC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);

	}
  	
    public function confirm() {

      	$id = isset($this->params['id']) ? $this->params['id'] : 0;

      	$lock = isset($this->params['lock']) ? $this->params['lock'] : -1;
      	
      	$is = false;
      
        $message = $this->lang->line('failure');
      
      	if($id > 0) {
    	
          	$lock >= 0 ? $this->db->set('is_lock', $lock) : $this->db->set('status', 1);
          	
        	$this->db->where('id', $id);
      
      		$is = $this->db->update($this->table);
          
        }
      
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
      
		$is == true ? 	$this->responsesuccess($message) :	$this->responsefailure($message);

	}
  
  
  	public function getListStudent(){
		
		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
      
      	$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;

		$sql=" SELECT t1.student_id, t1.is_videos, t2.name as student_name, t2.code
		
		FROM tb_student_product AS t1
        
        LEFT JOIN tb_student AS t2 ON t1.student_id = t2.id
		
		WHERE t1.class_id =". $class_id ." AND t1.product_id = ".$product_id." GROUP BY t1.student_id ORDER BY t1.maker_date ASC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);

	}
  	public function processVideos()
	{
      	$data= $this->getdata();
      
      	$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;
      
      	$product_id = isset($this->params['product_id']) ? $this->params['product_id'] : 0;
     
		$is = 0;

		$message = $this->lang->line('failure');

		if ($data !== null) {
          
            for($i = 0 ; $i < count($data); $i++){
              
              	$arrayData =  array('is_videos' => $data[$i]['is_videos']);
    
                $arrayWhere = array('class_id' => $class_id, 'product_id' => $product_id, 'student_id', 'student_id' => $data[$i]['student_id']);

                $this->db->set($arrayData);

                $this->db->where($arrayWhere);
             
              	$is = $this->db->update('tb_student_product');
            }
					
			$message = ($is == 1) ? $this->lang->line('success') : $this->lang->line('failure');
		}

		$is == 1 ? 	$this->responsesuccess($message) :	$this->responsefailure($message);
	}
	
	public function remove(){

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if($id > 0){

            $sql="SELECT COUNT(id) AS count FROM tb_class_product WHERE class_id=".$id;
          
            if($this->db->query($sql)->row_object()->count > 0){

                $this->db->where('class_id', $id);

                $is = $this->db->delete('tb_class_product');

                if($is){

                    $sql="SELECT COUNT(id) AS count FROM ".$this->table." WHERE id=".$id;	

                    if($this->db->query($sql)->row_object()->count == 1) {

                        $this->db->where('id', $id);

                        $is = $this->db->delete($this->table);

                    }
                }
            }
		}	

		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);

		} else {

			$this->responsefailure($message);
		}

	}

}
?>