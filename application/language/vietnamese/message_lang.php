<?php

//action 
$lang['success'] ="Quá trình yêu cầu của bạn đã thành công.";
$lang['failure'] ="Quá trình yêu cầu của bạn không thành công.";
$lang['isDelete'] = "Dữ liệu Không được xóa ( Dữ liệu mặc định ).";


//kiểm tra 
$lang['CheckNameGroup'] = "Tên nhóm đã tồn tại.";
$lang['CheckName'] = "Tên đã tồn tại.";
$lang['CheckKey'] = "key của bạn đã tồn tại.";
$lang['checkExitBrand'] = "Tên thương hiệu đã tồn tại.";
$lang['checkExitOrigin'] = "Nơi sản xuất đã tồn tại.";
$lang['checkExitrAttribute']="Thuộc tính sản phẩm đã tồn tại";
$lang['checkExitrValueAttribute']="Giá trị thuộc tính đã tồn tại";
$lang['checkExitnameSlide']="Tiêu đề Banner website đã tồn tại";

$lang['checkDeleteBrand']="(Có sản phẩm thuộc THƯƠNG HIỆU đã chọn)";
$lang['checkDeleteOrigin']="(Có sản phẩm thuộc NƠI SẢN XUẤT đã chọn)";
$lang['checkDeleteAttribute']="(Có sản phẩm cấu hình THUỘC TÍNH đã chọn)";


//User
$lang['checkExitEmail'] = "Email của bạn đã được sử dụng. Vui lòng kiểm tra lại!";
$lang['checkExitname'] = "Tên của bạn đã được sử dụng. Vui lòng kiểm tra lại!";
$lang['checkExitaddress'] = "Địa chỉ của bạn đã được sử dụng. Vui lòng kiểm tra lại!";
$lang['checkExitPhone'] = "Số điện thoại đã tồn tại. Vui lòng kiểm tra lại!";
$lang['successrepassword'] ="Yêu cầu thay đổi mật khẩu thành công!";
$lang['tokenFailure'] ="Xin lỗi TOKEN của bạn không đúng. Vui lòng liên hệ với người quản trị Website.";
$lang['mexExitAccount'] ="Xin lỗi tài khoản của bạn có thể bị khoá hoặc đã được xoá khỏi hệ thống. Vui lòng liên hệ với người quản trị Website.";
$lang['failLogin'] = "Email hoặc mật khẩu của bạn không đúng.";
$lang['loginSuccess'] = "Đăng nhập thành công";

//pages
$lang['pagesNameError'] = "Tên trang của bạn đã được sử dụng. Vui lòng kiểm tra lại!";
$lang['pagesGroupNameError'] = "Tên nhóm MENU của bạn đã được sử dụng. Vui lòng kiểm tra lại!";


//customer

$lang['exitCustomer'] = "Nhóm này đang tồn tại khách hàng! Vui lòng kiểm tra lại.";
$lang['exitEmail'] = "Email đã tồn tại! Vui lòng kiểm tra lại.";
$lang['failurePasswordOld'] = "Mật khẩu cũ không chính xác! Vui lòng kiểm tra lại.";
$lang['failLoginStudent'] = "Tài khoản hoặc mật khẩu không đúng! Vui lòng kiểm tra lại.";
$lang['codeCompanyNotExist'] = "Mã doanh nghiệp không tồn tại! Vui lòng kiểm tra lại.";

$lang['checkDeleteProduct'] = "(Sản phẩm đã có trong đơn hàng)";
$lang['checkDeleteDocument']="(Có sản phẩm cấu hình TÀI LIỆU SẢN PHẨM đã chọn)";
$lang['CheckEmail']= "Email đã tồn tại.";

// Languages module
$lang['checkExitCode']= "Mã Code ngôn ngữ đã tồn tại.";