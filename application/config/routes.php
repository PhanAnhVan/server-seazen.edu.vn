<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / DASHBOARD
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/

// $route['api/contact/registerCourse'] = 'api/registerCourse';
$route['get/dashboard/getlist'] = 'dashboard/getdashboard';
$route['get/dashboard/sitemap'] = 'dashboard/sitemap';
$route['get/dashboard/getUnRead'] = 'dashboard/getUnRead';
/** ================== END ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PAGES
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/pages/getlist'] = 'pages/getlist';
$route['get/pages/getlistsolution'] = 'pages/getlistsolution';
$route['get/pages/getrow'] = 'pages/getrow';
$route['get/pages/grouptype'] = 'pages/grouptype';
$route['set/pages/process'] = 'pages/process';
$route['set/pages/remove'] = 'pages/remove';
$route['set/pages/changestatus'] = 'pages/changestatus';
$route['set/pages/updateRelated'] = 'pages/updateRelated';
$route['set/pages/changePin'] = 'pages/changePin';
$route['get/pages/getlistproducts'] = 'pages/getListProducts';
$route['get/pages/getproductsservice'] = 'pages/getProductsService';
$route['set/pages/processProductService'] = 'pages/processProductService';
/** ================== END ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PAGE GROUP / MENU
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/pagesgroup/getlist'] = 'pagegroup/getlist';
$route['get/pagesgroup/getrow'] = 'pagegroup/getrow';
$route['set/pagesgroup/process'] = 'pagegroup/process';
$route['set/pagesgroup/remove'] = 'pagegroup/remove';
/** ================== END ==================**/

/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PRODUCTS
 * ------------------
 * Edit
 * Name:  LE CHIEN TRUC 
 * Date : 23/01/2021
 * Note : updateDevice
 *
 **/
$route['get/product/getlist'] = 'product/getlist';
$route['get/product/getrow'] = 'product/getrow';
$route['get/product/getdocument']='product/getdocument';
$route['set/product/process'] = 'product/process';
$route['set/product/remove'] = 'product/remove';
$route['set/product/updateImages'] = 'product/updateImages';
$route['set/product/updateDocument'] = 'product/updateDocument';
$route['set/product/syncDocument'] = 'product/syncDocument';

$route['set/product/updateAttribute'] = 'product/updateAttribute';
$route['set/product/updateSeo'] = 'product/updateSeo';
$route['set/product/updateVideos'] = 'product/updateVideos';
$route['set/product/updatePrice'] = 'product/updatePrice';
$route['set/product/update/priceattribute'] = 'product/updatePriceAtribute';
$route['set/product/remove/priceattribute'] = 'product/removePriceAtribute';

$route['set/product/updateDevice'] = 'product/updateDevice';
$route['set/product/changeHot'] = 'product/changeHot';
$route['set/product/changeStatus'] = 'product/changeStatus';
//cấu hình tai liệu sp
$route['get/productconfig/document/getlist'] = 'documentproducts/getlist';
$route['get/productconfig/document/getrow'] = 'documentproducts/getrow';
$route['set/productconfig/document/process'] = 'documentproducts/process';
$route['set/productconfig/document/remove'] = 'documentproducts/remove';
/** ================== END ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / BRAND ANH ORIGIN
 * ------------------
 * Edit
 * Name:  LE CHIEN TRUC 
 * Date : 23/01/2021
 * Note : updateDevice/ ORDER
 *
 **/
// order
$route['get/order/getlist'] = 'order/getlist';
$route['get/order/getrow'] = 'order/getrow';
$route['set/order/process'] = 'order/process';
$route['set/order/processDocumentProduct'] = 'order/processDocumentProduct';
$route['set/order/remove'] = 'order/remove';
$route['get/order/getListServices'] = 'order/getListServices';
$route['get/order/getListCustomers'] = 'order/getListCustomers';
$route['get/order/getDocumentProduct'] = 'order/getDocumentProduct';

// brand
$route['get/brand/getlist'] = 'brand/getlist';
$route['get/brand/getrow'] = 'brand/getrow';
$route['set/brand/process'] = 'brand/process';
$route['set/brand/remove'] = 'brand/remove';

// origin
$route['get/origin/getlist'] = 'origin/getlist';
$route['get/origin/getrow'] = 'origin/getrow';
$route['set/origin/process'] = 'origin/process';
$route['set/origin/remove'] = 'origin/remove';

// attribute 
$route['get/attribute/getlist'] = 'attribute/getlist';
$route['get/attribute/getlistall'] = 'attribute/getlistall';
$route['get/attribute/getrow'] = 'attribute/getrow';
$route['set/attribute/process'] = 'attribute/process';
$route['set/attribute/remove'] = 'attribute/remove';
$route['set/attribute/removevalue'] = 'attribute/removeValueAtribute';

// customer
$route['get/customer/getlist'] = 'customer/getlist';
$route['get/customer/getrow'] = 'customer/getrow';
$route['set/customer/process'] = 'customer/process';
$route['set/customer/remove'] = 'customer/remove';
$route['set/customer/changepassword'] = 'customer/changepassword';
$route['get/customer/getStudentLargestCode'] = 'customer/getStudentLargestCode';

/** ================== END ROUTER BRAND ANH ORIGIN ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / CONTENT
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/content/getlist'] = 'content/getlist';
$route['get/content/getrow'] = 'content/getrow';
$route['set/content/process'] = 'content/process';
$route['set/content/remove'] = 'content/remove';
$route['set/content/changepin'] = 'content/changepin';
$route['set/content/updateRelated'] = 'content/updateRelated';
/** ================== END ROUTER CONTENT ==================**/



/**
 * 
 * Create
 * Name: Phan Anh Van
 * Date: 23/08/2021
 * Note: ROUTER ADMIN / Registration class
 * ------------------
 * Edit
 * Name:  
 * Date:
 * Note: 
 *
 **/
$route['get/registrationclass/getAllClass'] = 'registrationclass/getAllClass';
$route['get/registrationclass/getAllStudent'] = 'registrationclass/getAllStudent';
$route['get/registrationclass/getCourseInClass'] = 'registrationclass/getCourseInClass';
$route['set/registrationclass/process'] = 'registrationclass/process';
$route['get/registrationclass/getlist'] = 'registrationclass/getlist';
$route['get/registrationclass/getrow'] = 'registrationclass/getrow';
$route['set/registrationclass/remove'] = 'registrationclass/remove';


/**
 * 
 * Create
 * Name: NGUYỄN NGỌC TOÀN
 * Date : 12/08/2021
 * Note: ROUTER ADMIN / class
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/class/getlist'] = 'classcourse/getlist';
$route['get/class/getrow'] = 'classcourse/getrow';
$route['set/class/process'] = 'classcourse/process';
$route['set/class/confirm'] = 'classcourse/confirm';
$route['set/class/remove'] = 'classcourse/remove';
$route['get/class/getCourse'] = 'classcourse/getCourse';
$route['get/class/getTeacher'] = 'classcourse/getTeacher';
$route['get/class/getListStudent'] = 'classcourse/getListStudent';
$route['set/class/processVideos'] = 'classcourse/processVideos';
/** ================== END ROUTER CONTENT ==================**/

/**
 * 
 * Create
 * Name: NGUYỄN NGỌC TOÀN
 * Date : 12/08/2021
 * Note: ROUTER ADMIN / score
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
 $route['get/score/getClass'] = 'score/getClass';
$route['get/score/getCourseClass'] = 'score/getCourseClass';
$route['get/score/getListStudent'] = 'score/getListStudent';
$route['set/score/process'] = 'score/process';
$route['set/score/lockCourse'] = 'score/lockCourse';
$route['set/score/endCourse'] = 'score/endCourse';
$route['set/score/calculateAVG'] = 'score/calculateAVG';


/** ================== END ROUTER CONTENT ==================**/


/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / CONTACT
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/

$route['get/contact/getlist'] = 'contact/getlist';
$route['api/contact/getAllMajors'] = 'contact/getAllMajors';
$route['get/contact/getrow'] = 'contact/getrow';
$route['set/contact/process'] = 'contact/process';
$route['set/contact/remove'] = 'contact/remove';
$route['set/contact/sendmail'] = 'contact/sendmailContact';
/** ================== END ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / USER
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/user/getlist'] = 'user/getlist';
$route['get/user/getrow'] = 'user/getrow';
$route['set/user/process'] = 'user/process';
$route['set/user/remove'] = 'user/remove';
$route['set/user/changepassword'] = 'user/changepassword';
$route['set/user/changeToken'] = 'user/changeToken';
$route['get/user/academics'] = 'user/getAcademics';
/**================== END ROUTER ADMIN / USER ==================**/

/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / PARTNER
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/partner/getlist'] = 'partner/getlist';
$route['get/partner/getrow'] = 'partner/getrow';
$route['set/partner/process'] = 'partner/process';
$route['set/partner/remove'] = 'partner/remove';
/**================== END ROUTER PARTNER ==================**/

/**
 * 
 * Create
 * Name: LE CHIEN TRUC   
 * Date : 23/01/2021
 * Note: ROUTER ADMIN / document
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/document/getlist'] = 'document/getlist';
$route['get/document/getrow'] = 'document/getrow';
$route['set/document/process'] = 'document/process';
$route['set/document/remove'] = 'document/remove';
/**================== END ROUTER ADMIN / USER ==================**/
/**
 * 
 * Create
 * Name: NGUYỄN VĂN TIÊN   
 * Date : 05/01/2021
 * Note: ROUTER ADMIN / SETTINGS
 * ------------------
 * Edit
 * Name:  
 * Date : 
 * Note : 
 *
 **/
$route['get/settings/emailgetrow'] = 'settings/emailgetrow';
$route['set/settings/email'] = 'settings/email';
$route['get/settings/getorther'] = 'settings/getorther';
$route['set/settings/setorther'] = 'settings/setorther';
$route['get/settings/settingAdmin'] = 'settings/settingAdmin';
$route['get/title/getrowcompany'] = 'title/getrowcompany';
$route['set/title/company'] = 'title/company';
$route['get/slide/getlist'] = 'slide/getlist';
$route['get/slide/getrow'] = 'slide/getrow';
$route['set/slide/process'] = 'slide/process';
$route['set/slide/remove'] = 'slide/remove';
/**
 * 
 * Create
 * Name: PHAN ANH VAN   
 * Date: 03/11/2021
 * Note: SETTINGS -> LANGUAGES MODULE
 * ------------------
 * Edit
 * Name:
 * Date: 
 * Note: 
 *
 **/
$route['get/languages/getlist'] = 'languages/getList';
$route['set/languages/process'] = 'languages/process';
$route['get/languages/getrow'] = 'languages/getrow';
$route['set/languages/changeStatus'] = 'languages/changeStatus';
$route['set/languages/remove'] = 'languages/remove';
/**================== END ROUTER ADMIN / SETTINGS ==================**/

/**
 *
 * Create
 * Name: PHAN ANH VAN
 * Date: 26/03/2021
 * Note: ADD GROUP DOCUMENT
 * ------------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/groupDocument/getlist'] = 'groupdocument/getlist';
$route['get/groupDocument/getrow'] = 'groupdocument/getrow';
$route['set/groupDocument/process'] = 'groupdocument/process';
$route['set/groupDocument/remove'] = 'groupdocument/remove';

/**
 *
 * Create
 * Name: PHAN ANH VAN
 * Date: 26/03/2021
 * Note: ADD TYPE DOCUMENT
 * -----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/typeDocument/getlist'] = 'typedocument/getlist';
$route['get/typeDocument/getrow'] = 'typedocument/getrow';
$route['set/typeDocument/process'] = 'typedocument/process';
$route['set/typeDocument/remove'] = 'typedocument/remove';

/**
 *
 * Create
 * Name: PHAN ANH VAN
 * Date: 26/03/2021
 * Note: ADD FIELD
 * -----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/field/getlist'] = 'field/getlist';
$route['get/field/getrow'] = 'field/getrow';
$route['set/field/process'] = 'field/process';
$route['set/field/remove'] = 'field/remove';


/**
 *
 * Create
 * Name: Nguyễn ngọc toàn
 * Date: 26/03/2021
 * Note: ADD evaluate
 * -----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/evaluate/getlist'] = 'evaluate/getlist';
$route['set/evaluate/remove'] = 'evaluate/remove';
$route['set/evaluate/changetype'] = 'evaluate/changetype';
$route['get/evaluate/getrow'] = 'evaluate/getrow';
$route['set/evaluate/process'] = 'evaluate/process';
/**
 *
 * Create
 * Name: PHAN ANH VAN
 * Date: 26/03/2021
 * Note: ADD AGENCIES
 * -----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/agencies/getlist'] = 'agencies/getlist';
$route['get/agencies/getrow'] = 'agencies/getrow';
$route['set/agencies/process'] = 'agencies/process';
$route['set/agencies/remove'] = 'agencies/remove';

/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: ROUTER PAGE HOME
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/home/aboutUs'] = 'api/getAboutUs';
$route['api/home/content'] = 'api/contentHome';
$route['api/home/partner'] = 'api/partnerHome';
/**
*
* Create
* Name: NGUYỄN NGỌC TOÀN
* Date : 25/01/2021
* Note: ROUTER PAGE HOME
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/home/getLanguages'] = 'api/getLanguages';
$route['api/home/getHeaderInfo'] = 'api/getHeaderInfo';
$route['api/home/getCategory'] = 'api/getCategory';
$route['api/home/getProduct'] = 'api/getProductHome';
$route['api/home/getService'] = 'api/getServiceHome';
$route['api/home/getGroupProduct'] = 'api/getGroupProductHome';
$route['api/home/getServiceFooter'] = 'api/getServiceFooter';
$route['api/home/getPartner']='api/getPartnerHome';
$route['api/home/getEvaluate'] = 'api/evaluateHome';
/**================== END ROUTER PAGE HOME ==================**/

/**
 * Create
 * Name: Phan Anh Van
 * Date : 12/04/2021
 * Note: GET LIST SERVICE
 * ----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 */
$route['api/service/list'] = 'api/serviceList';
/**================== END ==================**/

/**
 * Create
 * Name: Phan Anh Van
 * Date : 15/04/2021
 * Note: api getListDocument
 * ----------------------
 * Edit
 * Name:
 * Date:
 * Note:
 */
$route['api/getListDocument'] = 'api/getListDocument';
$route['api/getSearchDocument'] = 'api/getSearchDocument';

// Notification
$route['api/getNotification'] = 'api/getNotification';
/**================== END ==================**/
/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: ROUTER PAGE CONTENT
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/content/list'] = 'api/contentList';
$route['api/content/group'] = 'api/getContentGroup';
// $route['api/content/listGroup'] = 'api/getContentListGroup';
$route['api/content/asidebarRight'] = 'api/getNewsAsidebarRight';
$route['api/content/detail'] = 'api/getContentDetail';

/**================== END ==================**/

/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: ROUTER PAGE PRODUCTS / DETAIL
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
/**
*
* Create
* Name: NGUYỄN NGỌC TOÀN
* Date : 25/01/2021
* Note: ROUTER getCategory
* ------------------
* Edit
* Name: NGUYỄN NGỌC TOÀN
* Date : 25/01/2021
* Note : getProduct,getProductDetail
*
**/
$route['api/products/list'] = 'api/getProduct';
$route['api/products/detail'] = 'api/getProductDetail';
$route['api/products/getCategory'] = 'api/getCategory';
/**================== END ROUTER PAGE PRODUCTS / DETAIL ==================**/

/**
*
* Create
* Name: NGUYỄN NGỌC TOÀN
* Date : 26/01/2021
* Note: ROUTER search
* ------------------
* Edit
* Name: 
* Date : 
* Note : 
*
**/
$route['api/page/search'] = 'api/search';
$route['api/search'] = 'apisearch/search';
/**================== END ROUTER SEARCH ==================**/
/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: ROUTER API QUETTIN
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/processApi'] = 'api/processApi';
/**================== END ROUTER PAGE PRODUCTS / DETAIL ==================**/
/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: API CLIENT / CUSTOMER
* ------------------
* Edit
* Name: PHAN ANH VAN
* Date : 12/04/2021 
* Note : LINE api/registration/customer
*
**/
$route['api/customer/logout'] = 'apicustomer/logout';
$route['api/customer/checkLogin'] = 'apicustomer/checkcustomer';
$route['api/login/customer'] = "apicustomer/loginCustomer";
$route['api/customer/addCart'] = "apicustomer/addCart";
$route['api/customer/info'] = "apicustomer/infoCustomer";
$route['api/customer/update'] = "apicustomer/updateCustomer";
$route['api/customer/changePasswordCustomer'] = "apicustomer/changePasswordCustomer";
$route['api/customer/orders']='apicustomer/listOrder';
$route['api/customer/ordersdetail']='apicustomer/getCartDetail';
$route['api/customer/getListProducts'] = "apicustomer/getListProducts";
$route['api/customer/getDetailProduct'] = "apicustomer/getProductCustomer";
$route['api/customer/getDocumentInfo'] = "apicustomer/getDocumentInfo";
$route['api/customer/registration'] = "apicustomer/registration";
$route['api/customer/resetPassword'] = "apicustomer/resetPassword";
$route['api/customer/checktoken'] = "apicustomer/checkTokenResetPassword";
$route['api/customer/loginfacegoogle'] = "apicustomer/loginfacegoogle";
$route['api/customer/companySignin'] = "apicustomer/companySignin";
$route['api/customer/getAllStudentByCompany'] = "apicustomer/getAllStudentByCompany";


/**================== END ==================**/
/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: ROUTER API CART
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/cart/payment']= "apicustomer/paymentCart";

/**================== END ==================**/


/*
 *
 * Create
 * Name: PHAN ANH VAN
 * Date : 09/09/2021
 * Note: GET ALL CLASS
 * -------------------
 * Edit
 * Name:
 * Date:
 * Note:
 *
 */
$route['get/list/class']= "apicustomer/getListClass";
$route['get/list/videos']= "apicustomer/getCourseVideos";


/**
*
* Create
* Name: NGUYỄN VĂN TIÊN
* Date : 05/01/2021
* Note: API SETTINGS WEBSITE
* ------------------
* Edit
* Name:
* Date :
* Note :
*
**/
$route['api/contact/add'] = 'api/addContact';
$route['api/home/slide'] = 'api/getSlide';
$route['api/pages/detail'] = 'api/pageDetail';
$route['api/setting/language'] = 'api/setting';
$route['api/getmenu'] = 'api/getmenu';
$route['api/company'] = 'api/company';
$route['api/login/check'] = 'api/checklogin';
$route['api/login/admin'] = "api/loginAdmin";
$route['api/logout/admin'] = "api/logoutAdmin";
$route['(.*)'] = $route['default_controller'];
$route['translate_uri_dashes'] = FALSE;
/**================== END ==================**/