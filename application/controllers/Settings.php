<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Settings extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->table = "wstm_setting";

		$this->history = './public/history/settings/';

		$this->settings = './public/settings/';

		if (!is_dir($this->history)) {

			mkdir($this->history, 0757);
		}
		if (!is_dir($this->settings)) {

			mkdir($this->settings, 0757);
		}
	}
	
	public function settingAdmin()
	{

		$sql = "SELECT id, text_key, title, value, type, status 
		FROM wstm_setting Where admin = 1";

		$query = $this->db->query($sql);
		$list = $query->result_object();
		$list = ($list != null) ? $list :  array();
		$message = $this->lang->line('success');
		$this->responsesuccess($message, $list);
	}
	public function emailgetrow()
	{

		$mail = $this->config->item('config_mail');

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $mail);
	}
	public function email()
	{
		$data = $this->getdata();

		$this->load->library('config_write');

		$writer = $this->config_write->get_instance();

		$is = $writer->write('config_mail', $data);

		$this->responsesuccess($this->lang->line('success'));
	}
	
	public function getorther()
	{

		$group_setting = isset($this->params['group']) ? $this->params['group'] : 0;

		$sql = "SELECT t1.id, t1.title, t1.text_key, t1.value, t1.type, t1.status 
		
		FROM " . $this->table . " AS t1 WHERE t1.group_setting = ".$group_setting." AND status = 1 ORDER BY t1.orders ASC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function setorther()
	{

		$data = $this->getdata();

		$is = false;

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($data !== null) {

			$key = $data['text_key'];

			if ($data['type'] == 2) {

				if (array_key_exists('value', $data)) {

					$data['value'] = $this->processimages($data['value']);
				}
			}

			$id = $id > 0 ? $id : (array_key_exists('id', $data) ? (int) $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$sql = "SELECT count(id) as count FROM wstm_setting WHERE text_key = '" . $key . "'";

			if ($id != 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('CheckKey');
			}
		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
