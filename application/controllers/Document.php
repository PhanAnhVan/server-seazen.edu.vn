<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends MY_Controller {
	
	public function __construct(){
		
		parent:: __construct();

		$this->table = "tb_document";
		
	}
	public function getlist(){
	
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$sql="SELECT t1.id, t1. code, t1.name, t1.status, 
		
		t1.maker_id, t1.maker_date , t1.start_date, t1.end_date
		
		FROM ".$this->table." AS t1  ORDER BY t1.maker_date DESC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}
	
	public function getrow(){

		$id = $this->params['id'];

		$sql="SELECT * FROM ".$this->table." WHERE id=".$id;

		$query=$this->db->query($sql);

		$data = $query->row_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}
	
	public function process(){
		
		$data = $this->getdata();
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			if (array_key_exists('file', $data)) {

				$data['file'] = $this->processimages($data['file']);
				
			}else{
				
				$data['file'] = '';
			}
			
			$id = $id >0 ? $id : (array_key_exists('id',$data) ? (int) $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');
		
			$sql ="SELECT COUNT(id) AS count FROM ".$this->table." WHERE name='".$data['name']."'";
	
			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}
				
			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {
					
					$data['maker_date'] = date('Y-m-d H:i:s');

					$is = $this->db->insert($this->table, $data);
					
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}
			}

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			 
		}
		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}

	public function remove(){
		
		$data = $this->getdata();

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		$message = $this->lang->line('failure');
		
		if($id > 0){

			$sql="select * from ".$this->table." where id=".$id;
			
			$list = $this->db->query($sql)->row_object();
						
			$data['maker_date']= date('Y-m-d H:i:s');
			
			$this->db->where('id', $id);
			
			$is = $this->db->delete($this->table);
			
			if(isset($list->images) && strlen($list->images) > 4){
				
				@unlink('public/contents/'.$list->images);
			}
		}
				
		$message = ($is==true) ? $this->lang->line('success') : $this->lang->line('failure');
		
		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
}
 
?>