<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pages extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "wstm_page";
	}

	public function getlist()
	{

		$type = isset($this->params['type']) ? $this->params['type'] : 0;

		$sql = "SELECT t1.id , t1.name ,  t1.link , t1.type , t1.parent_id , t1.link_video, t1.orders, t1.pin , t1.status , t1.maker_date, t2.name AS parent_name
		
		FROM " . $this->table . " AS t1 LEFT JOIN " . $this->table . " AS t2 ON t1.parent_id = t2.id" ;

		if ($type && $type > 0) {					
			
			$sql .= " WHERE t1.type = " . $type;
			
		}
		$sql .= " ORDER BY t1.maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$list = ($list != null) ? $list :  array();

		$list = array_reduce($list, function ($n, $o) {
			$n[$o->id] = $o;
			return $n;
		});

		if (!empty($list)) {

			foreach ($list as $key => $value) {

				if (array_key_exists($value->parent_id, $list)) {

					$list[$key]->parent_name = $list[$value->parent_id]->name;

					$list[$key]->parent_link = $list[$value->parent_id]->link;
				}
			};
		}

		@$list = array_values($list);

		$this->responsesuccess(null, $list);
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess(null, $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = $this->params['id'];
        
		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('images', $data)  && is_array($data['images'])) {
             
				 $data['images'] = $this->processimages($data['images']);
				 
			} else {

				$data['images'] = '';
			}
			if (array_key_exists('listimages', $data) && is_array($data['listimages'])) {

				$data['listimages'] = $this->processimages($data['listimages']);
				
			}else {
				
				$data['listimages'] ='';
			}
			    
			if (array_key_exists('icon', $data) && is_array($data['icon'])) {

				$data['icon'] = $this->processimages($data['icon']);
				
			}else {
				
				$data['icon'] ='';
			}

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$data['link'] = $data['type'] != 1 ? ((array_key_exists('link', $data) && strlen($data['link']) > 0) ? removesign($data['link']) : removesign($data['name'])) : $data['link'];
			
			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE type = " . $data['type'] . " AND link='" . $data['link'] . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);
					
					$id = $this->db->insert_id();
					
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}
				
				

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('pagesNameError');
			}
		} else {

			$message = $this->lang->line('failure');
		}
		
		
		
		$is == true ? $this->responsesuccess($message, array( 'id' => $id) ) : $this->responsefailure($message);
		
	}
	
	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE  is_delete = 1 AND id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 0 && $this->checkedRemove($id)) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('isDelete');
			}
		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function checkedRemove($id)
	{

		$skip = false;

		$sql = "SELECT count(id) as count FROM wstm_page WHERE  parent_id = " . $id;

		if ($this->db->query($sql)->row_object()->count == 0) {

			$sql = "SELECT count(id) as count FROM pdtb_product WHERE  page_id = " . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$sql = "SELECT count(id) as count FROM wstm_content WHERE  page_id = " . $id;

				if ($this->db->query($sql)->row_object()->count == 0) {

					$sql = "SELECT count(id) as count FROM wstb_library WHERE  page_id = " . $id;

					if ($this->db->query($sql)->row_object()->count == 0) {

						$skip = true;
					}
				}
			}
		}

		return $skip;
	}
	public function grouptype()
	{

		$type = isset($this->params['type']) && $this->params['type'] > 0 ? $this->params['type'] : 0;

		$sql = "SELECT t1.id, t1.name, t1.link , t2.count_library, t3.count_product , t4.count_content FROM wstm_page as t1

        LEFT JOIN ( SELECT count(id) as count_library , page_id   FROM wstb_library GROUP BY page_id)  as t2 On t1.id = t2.page_id

		LEFT JOIN ( SELECT count(id) as count_product , page_id   FROM pdtb_product GROUP BY page_id)  as t3 On t1.id = t3.page_id

		LEFT JOIN ( SELECT count(id) as count_content , page_id   FROM wstm_content GROUP BY page_id)  as t4 On t1.id = t4.page_id

		WHERE t1.type = " . $type . " AND t1.status = 1 AND t1.id not in (select parent_id from wstm_page ) AND t1.parent_id != 0";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}
	
	public function changePin(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('pin' => $pin, 'maker_date' => date('Y-m-d H:i:s')));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
		
	}
	
	public function changestatus(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$status = isset($this->params['status']) ? $this->params['status'] : 0;
		
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('status' => $status));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
	}
	
	public function getlistsolution()
	{

		$type = isset($this->params['type']) ? $this->params['type'] : 0;

		$sql = "SELECT t1.id , t1.name ,  t1.link , t1.type  , t1.status 
		
		FROM " . $this->table . " AS t1 WHERE t1.parent_id = 35" ;

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}
    public function getListProducts(){

        $sql = "SELECT t1.id, t1.name, t1.price, t1.page_id, t2.name AS group_name

        FROM pdtb_product AS t1

        LEFT JOIN " . $this->table . " AS t2 ON t1.page_id = t2.id

        WHERE t1.status = 1";

        $query = $this->db->query($sql);

        $result = $query->result_object();

        $this->responsesuccess(null, $result);
	}
	
	public function getProductsService(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$sql = "SELECT name, related FROM ".$this->table." WHERE id = ". $id;
		
		$query = $this->db->query($sql);
		
		$result = $query->row_object();
				
		$this->responsesuccess(null, $result);
	}
	
	public function processProductService(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$data = $this->getdata();
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('related' => $data['related']));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
	}
}
