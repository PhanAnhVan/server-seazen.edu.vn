<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrationclass extends MY_Controller {
	
	public function __construct(){

		parent:: __construct();

		$this->table = "tb_student_product";
	}

	public function getAllClass() {

		$sql="SELECT id, name FROM tb_class WHERE status = 1 AND is_lock = 0 ORDER BY maker_date DESC";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function getCourseInClass() {

		$id = $this->params['id'];

		$sql = "SELECT t2.id, t2.name, t2.page_id, t1.class_id, t2.score_pass, t3.score

				FROM tb_class_product AS t1

				LEFT JOIN pdtb_product AS t2 ON t1.product_id = t2.id

				LEFT JOIN tb_student_product AS t3 ON (t3.product_id = t1.product_id AND t3.class_id = t1.class_id)
				
				WHERE t1.class_id = ".$id." AND t1.status = 1
                
                GROUP BY t2.id
				
				ORDER BY t2.maker_date DESC";
                
	
		$data = $this->db->query($sql)->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);

	}

	public function getAllStudent() {

		$sql="SELECT code, id, name, sex, phone, address FROM tb_student WHERE status = 1 ORDER BY maker_date";
	
		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function process() {

		$data = $this->getdata();

		$student_id = isset($this->params['student_id']) ? $this->params['student_id'] : 0;

		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;

		$is = 0;

		$message = $this->lang->line('failure');

		if ($data !== null) {

			if ($student_id == 0) {

				$sql = "SELECT student_id from tb_student_product WHERE class_id = ".$class_id." GROUP BY student_id";

				$query = $this->db->query($sql);

				$result = $query->result_object();

				$count = 0;

				$arrayExist = array();

				for($i = 0 ; $i < count($data['student']); $i++) {
				
					for($j = 0 ; $j < count($result); $j++) {

						if($data['student'][$i]['student_id'] == $result[$j]->student_id) {

							$count++;

							array_push($arrayExist, $result[$j]->student_id);

						}
					}
				}

				if($count > 0) {

					$is = -1;
				
				} else {

					for($i = 0 ; $i < count($data['course']); $i++) {

						for($j = 0 ; $j < count($data['student']); $j++) {

							$data['student'][$j]['product_id'] = $data['course'][$i]['id'];

							$data['student'][$j]['score_pass'] = $data['course'][$i]['score_pass'];

							$data['student'][$j]['maker_date'] = date('Y-m-d H:i:s');
							
							$data['student'][$j]['maker_id'] = $this->session->userdata('user_id');

							$is = $this->db->insert('tb_student_product', $data['student'][$j]);
						}
					}
				}

			} else {

				$sql = "SELECT COUNT(product_id) AS count_product, student_id, class_id, page_id from tb_student_product 

						WHERE class_id = " .$class_id. " AND student_id = " .$student_id;	

				$query = $this->db->query($sql);

				$result_check = $query->row_object();

				if($result_check->count_product > count($data['course'])) {

					$sql = "DELETE FROM tb_student_product WHERE class_id = " .$class_id. " AND student_id = " .$student_id." AND product_id NOT IN (";	
					for($i = 0 ; $i < count($data['course']); $i++) {

						$sql .= " ".$data['course'][$i]['id']." ";

						if($i < count($data['course']) -1 ){

							$sql .= " , ";

						}
					}

					$sql .=")";

					$is = $this->db->query($sql);


				} else {

					for($i = 0 ; $i < count($data['course']); $i++) {

						$sql = "SELECT product_id from tb_student_product WHERE class_id = " .$class_id. "

						AND student_id = " .$student_id. " AND product_id = ".$data['course'][$i]['id'];

						$query = $this->db->query($sql);

						$result = $query->row_object(); 

						if(count((array)$result) == 0) {

							$array_update = array('class_id' => $result_check->class_id, 'student_id' => $result_check->student_id, 'page_id' => $result_check->page_id, 

								'product_id' => $data['course'][$i]['id'], 'score_pass' => $data['course'][$i]['score_pass'], 'maker_date' =>date('Y-m-d H:i:s'),

								 'maker_id' => $this->session->userdata('user_id'), 'status' => 1);

							$is = $this->db->insert('tb_student_product', $array_update);

						} 
					}
				}
			}

			// $message = ($is == 1) ? $this->lang->line('success') : $this->lang->line('failure');
		}
	
		$is == 1 ? $this->responsesuccess($this->lang->line('success')) : ($is == -1 ? $this->responsefailure($this->lang->line('checkRegisteredStudent'), $arrayExist) : $this->responsefailure($this->lang->line('failure')));
	}

	public function getlist() {

      	$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;

		$sql = "SELECT t1.class_id, t1.student_id, t1.maker_date, t2.name, t2.sex, t2.phone, t3.name AS class_name
		
		FROM ".$this->table." AS t1 
		
		LEFT JOIN tb_student AS t2 ON t1.student_id = t2.id

		LEFT JOIN tb_class AS t3 ON t1.class_id = t3.id";

		if($class_id > 0) {
          	
          	$sql .=" WHERE t1.class_id = " .$class_id;
          
        }

        $sql .=" GROUP BY t1.class_id, t1.student_id ORDER BY t1.maker_date DESC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function getrow() {

		$class_id = isset($this->params['class_id']) ? $this->params['class_id'] : 0;

		$student_id = isset($this->params['student_id']) ? $this->params['student_id'] : 0;

		$sql = "SELECT t2.name AS class_name, t3.name AS student_name from tb_student_product AS t1 

				LEFT JOIN tb_class AS t2 ON t1.class_id = t2.id

				LEFT JOIN tb_student AS t3 ON t1.student_id = t3.id

				WHERE t1.student_id = ".$student_id." AND t1.class_id = ".$class_id;

		$query = $this->db->query($sql);

		$data = $query->row_object();

		$sql="SELECT product_id from tb_student_product WHERE student_id = ".$student_id." AND class_id = ".$class_id;
	
		$query = $this->db->query($sql);

		$data->products = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}


	public function remove() {

		$student_id = $this->params['student_id'];

		$class_id = $this->params['class_id'];

        if ($student_id > 0) {

			$sql = "SELECT count(student_id) as count FROM " . $this->table . " WHERE student_id = " . $student_id ." AND class_id = " . $class_id;

			if ($this->db->query($sql)->row_object()->count > 0) {

				$this->db->where('student_id', $student_id);

				$this->db->where('class_id', $class_id);

				$this->db->delete($this->table);

				$this->responsesuccess($this->lang->line('success'));

			} else {

				$this->responsefailure($this->lang->line('failure'));
			}
		} 
	}

}
?>