<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}
	/**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN  
     * Date: 05/01/2021
     * Note: API LOGIN / LOGOUT / CHECK LOGIN
     * ------------------
     * Edit
     * Name:
     * Date:
     * Note:
     *
     **/
    public function logoutAdmin()
    {

        @$this->session->sess_destroy();

        $this->responsesuccess(null, array());
    }

    public function loginAdmin()
    {

        $data = $this->getdata();

        $is = false;

        $password = isset($data['password']) ? sha1($data['password']) : '';

        $email = isset($data['email']) ? $data['email'] : '';

        $sql = "SELECT count(id) as count FROM hrtb_user WHERE email='" . $email  . "' AND status = 1 AND password ='" . $password . "'";

        if ($this->db->query($sql)->row_object()->count == 1) {

            $sql = "SELECT * FROM hrtb_user WHERE  status = 1 AND email='" . $data['email'] . "'  AND password ='" . $password . "'";

            $query = $this->db->query($sql);

            $list = $query->row_object();

            $this->session->set_userdata('user', true);

            $this->session->set_userdata('user_id', $list->id);

            $this->session->set_userdata('user_name', $list->name);

            $is = true;
        }

        $is == true ? $this->responsesuccess($this->lang->line('loginSuccess'), $list) : $this->responsefailure($this->lang->line('failLogin'));
    }

    public function checklogin()
    {

        $data = $this->getdata();
		
        $list = null;

        $skip = false;

        if ($data !== null) {

            $password = isset($data['password']) ? $data['password'] : '';

            $email = isset($data['email']) ? $data['email'] : '';

           $sql = "SELECT * FROM hrtb_user WHERE email='" . $email . "' AND status=1 AND password ='" . $password . "'";

            $query = $this->db->query($sql);

            $list = $this->db->query($sql)->row_object();

            if (!empty($list)) {
                
                $skip = true;
                
                $this->session->set_userdata('user', true);
                
                $this->session->set_userdata('user_id', $list->id);
                
                $this->session->set_userdata('user_name', $list->name);
            }
        }

        $list = array('skip' => $skip, 'data' => $list);
        
        echo json_encode($list);
    }
    /**================== END API LOGIN / LOGOUT / CHECK LOGIN ==================**/

    /**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN   
     * Date : 05/01/2021
     * Note: API SETTINGS WEBSITE
     * ------------------
     * Edit
     * Name:  
     * Date : 
     * Note : 
     *
     **/
    public function pageDetail()
    {

        $data = array();

        $url = base_url() . 'public/pages/';

        $link = isset($this->params['link']) ? $this->params['link'] : '';
        
        // tai-sao-bao-ve-cung-can-phai-hoc

        $link = $link == '\''  ? '' : $link;
        
        $parent_link = isset($this->params['parent_link']) ? $this->params['parent_link'] : '';
		
		$sql = "SELECT t1.id, t1.link, t1.description, t1.type, t1.name, t1.detail, t1.title, t1.related,

		(CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,

		t2.name as parent_name, t2.link as parent_link
			
 		FROM wstm_page AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
		
		WHERE t1.status = 1 AND t1.link = '".$link."' " ;

		if($parent_link != '') {

			$sql .= " AND (t2.link = '".$parent_link."' OR t3.link = '".$parent_link."')";
		}
 		
        $query = $this->db->query($sql);

        $data = $query->row_object();

        if(!empty($data) && $data->id > 0){
		
			$related = json_decode($data->related != null ? $data->related : '[]');
			
			$data->related = array();
			
			if(count($related) > 0){
				
				$related = array_reduce($related, function ($n, $o) { $n[+$o] = $o; return $n; }, []);
				
				$urlProduct = base_url() . 'public/products/';
			
				$type = isset($this->params['type']) ? $this->params['type'] : '';
				
				$limit = isset($this->params['limit']) ? $this->params['limit'] : 0;

				$sql = "SELECT t1.name, t1.link, (CASE WHEN t1.images !='' THEN CONCAT('" . $urlProduct . "', t1.images) ELSE '' END) as images,
				
				t1.price, t1.price_sale, t1.percent, t2.link as parent_link
				
				FROM pdtb_product AS t1 
				
				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
				WHERE t1.status = 1 AND t2.type = 3 AND t1.id IN (". implode(",", (array)$related). ")
				
				ORDER BY t1.maker_date DESC";
				
				$query = $this->db->query($sql);
				
				$data->related = $query->result_object();
			}
			
			if ($data->type == 6 ) {

	        	$sql = "SELECT t1.name, t1.link, t1.description, t1.link_redirect, t2.link AS parent_link,  

				(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images FROM wstm_page AS t1

	    		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id

	    		WHERE t1.parent_id = ".$data->id." AND t1.status = 1 AND t1.type = 6

	    		ORDER BY t1.id ASC";

	    		$query = $this->db->query($sql);

	            $data->service = $query->result_object();
	        }

	    } else {

	    	$sql = "SELECT count(t1.id) as count FROM pdtb_product AS t1 

		    		LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id
		    	
		    		WHERE t1.link = '". $link ."' AND t2.link = '". $parent_link ."' ";

		    if($this->db->query($sql)->row_object()->count == 1) {

		    	$data = (object)$data;

				$data->type = 6;
		    }

		    $sql = "SELECT count(t1.id) as count FROM wstm_content AS t1 

		    		LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id
		    	
		    		WHERE t1.link = '". $link ."' AND t2.link = '". $parent_link ."' ";

		    if($this->db->query($sql)->row_object()->count == 1) {

		    	$data = (object)$data;

				$data->type = 7;
		    }
		    
		    // $sql = "SELECT count(t1.id) as count FROM wstm_content AS t1 

		    // 		LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id
		    	
		    // 		WHERE t1.link = '". $link ."' AND t2.link = '". $parent_link ."' ";

		    // if($this->db->query($sql)->row_object()->count == 1) {

		    // 	$sql = "SELECT t1.link, t2.link AS parent_link, t2.type as type FROM wstm_content AS t1 

		    // 		LEFT JOIN wstm_page AS t2 ON t2.id = t1.page_id
		    	
		    // 		WHERE t1.link = '". $link ."' AND t2.link = '". $parent_link ."' ";

   			// 	$data = $this->db->query($sql)->row_object();

				// $url = base_url() . 'public/contents/';

		  //       $url_file = base_url() . 'public/file/';

		  //       $sql = "UPDATE wstm_content SET views = views + 1  WHERE link = '" . $link . "'";

		  //       $this->db->query($sql);

		  //       $sql = "SELECT t1.id, t1.name, t1.description, t1.views, t1.link, t1.author,
				
				// 		t2.name as parent_name, t2.link as parent_link, t3.link as parent_links, t3.name as parent_names, 

				// 		t1.page_id, t1.detail, t1.maker_date, t1.file AS name_file, t1.source, t1.keywords,

				// 		(CASE WHEN t1.file!='' THEN CONCAT('" . $url_file . "', t1.file) ELSE '' END) AS link_file,  
						
				// 		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images 
								
				// 		FROM wstm_content as t1 
						
				// 		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id 
						
				// 		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id 
						
				// 		WHERE t1.link='" . $link . "' AND t1.status = 1";

		  //       $query = $this->db->query($sql);

		  //       $data = $query->row_object();

		    // }

        }

        $this->responsesuccess($this->lang->line('success'), $data);
    }

    public function getmenu()
    {

        $position = $this->params['position'];

        $url = base_url() . 'public/pages/';

        $listid = array();

        $sql = "SELECT container FROM wstm_page_group WHERE position='" . $position . "' AND status=1";

        $query = $this->db->query($sql);

        $list = $query->row_object();

        if ($list) {

            $listid = (strlen($list->container) > 1) ? json_decode($list->container) : array();
        }

        $result = array();

        if (count($listid) > 0) {

            $sql = "SELECT t1.id ,t1.name, t1.description,t1.title, t1.type, t1.parent_id, t1.link_redirect,

			(CASE WHEN t1.orders = 0 THEN t1.id ElSE t1.orders END) AS orderby,
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
			
			(CASE WHEN t1.type != 1 && t2.link != '' THEN CONCAT(t2.link, '/', t1.link) ElSE CONCAT(t1.link) END) AS link
			
			FROM wstm_page  AS t1
			
			LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
			
			WHERE t1.id IN (" . implode(',', $listid) . ") AND t1.status=1 ORDER BY orderby ASC";

            $query = $this->db->query($sql);

            $result = $query->result_object();
        }

        $this->responsesuccess($this->lang->line('success'), $result);
    }

    public function getSlide()
    {

        $url = base_url() . 'public/slides/';

        $type = isset($this->params['type']) ? $this->params['type'] : 1;

        $sql = "SELECT id, name, link, title, description, orders, type, status , position, color, background,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images
		
		FROM wstm_slide WHERE status = 1 AND type = " . $type . " ORDER BY orders ASC ";

        $query = $this->db->query($sql);

        $list = $query->result_object();

        $message = $this->lang->line('success');

        $this->responsesuccess($message, $list);
    }

    public function company()
    {

        $url = base_url() . 'public/website/';

        $sql = "SELECT id, name, description, keywords, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) as logo, 
		
		(CASE WHEN logo_white!='' THEN CONCAT('" . $url . "', logo_white) ELSE '' END) as logo_white,
		
		(CASE WHEN shortcut!='' THEN CONCAT('" . $url . "', shortcut) ELSE '' END) as shortcut 
		
		FROM wstm_title WHERE id=1 AND status = 1";

        $query = $this->db->query($sql);

        $data = $query->row_object();

        $this->responsesuccess($this->lang->line('success'), $data);
    }

    public function setting()
    {

        $status = isset($this->params['status']) ? $this->params['status'] : 1;

        $sql = "SELECT id, text_key, title, value, type, status 
        
		        FROM wstm_setting WHERE status = ".$status;

        $query = $this->db->query($sql);

        $list = $query->result_object();

        $list = ($list != null) ? $list :  array();

        $message = $this->lang->line('success');

        $this->responsesuccess($message, $list);
    }

    /**================== END API SETTINGS WEBSITE ==================**/
	
	/**
	 * 
	 * Create
	 * Name: NGUYỄN NGỌC TOÀN
	 * Date : 25/01/2021
	 * Note: ROUTER PAGE HOME
	 * ------------------
	 * Edit
	 * Name:  
	 * Date : 
	 * Note : 
	 *
	 **/
	
	
	public function getProductHome(){

		$url = base_url() . 'public/products/';
		
		$type = isset($this->params['type']) ? $this->params['type'] : '';
		
		$limit = isset($this->params['limit']) ? $this->params['limit'] : 0;

        $sql = "SELECT t1.name, t1.link, (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		
		t1.price, t1.price_sale, t1.percent, t2.name as parent_name, t2.link as parent_link, t1.views, t1.maker_date
		
		FROM pdtb_product AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		WHERE t1.status = 1 AND t2.type = 3 ";
		
		if($type == 'hot'){
			
			$sql .= " AND t1.hot = 1";
		}
		
		$sql .= " ORDER BY t1.maker_date DESC LIMIT ".$limit;

        $query = $this->db->query($sql);

        $data = $query->result_object();

        $this->responsesuccess($this->lang->line('success'), $data);
    }
	
	public function getServiceHome(){

        $url = base_url() . 'public/pages/';

        $sql = "SELECT t1.id, t1.name, t1.link, t1.link_redirect, t1.description, t2.link as parent_link, 

		        (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		        
		        if(t1.orders > 0, t1.orders, t1.id) AS ordersby

				FROM wstm_page AS t1 

				LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id

				WHERE t1.status = 1 AND t2.type = 6 AND t1.pin = 1 ORDER BY ordersby ASC, t1.maker_date DESC LIMIT 3";

        $query = $this->db->query($sql);

        $data = $query->result_object();

        $this->responsesuccess($this->lang->line('success'), $data);
    }

	public function getPartnerHome(){
		$url = base_url() . 'public/partner/';

		$sql = "SELECT company, link, (CASE WHEN logo!='' THEN CONCAT('" . $url . "', logo) ELSE '' END) AS logo 
	
		FROM tb_partner WHERE status = 1 ORDER BY maker_date DESC ";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}
	
	public function evaluateHome(){
	    $url = base_url() . 'public/pages/';
	    
	    $url2 = base_url() . 'public/evaluate/';
		
		$sql = "SELECT id, name, link, description,title, link_video, detail,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images
		
		FROM wstm_page WHERE status =1 AND id= 33 ";
		
		$query = $this->db->query($sql);
		
		$data = $query->row_object();
		
		if(!empty($data)){
			
			$sql = "SELECT  info, name, address, sex, link,
	
    		(CASE WHEN logo!='' THEN CONCAT('" . $url2 . "',logo) ELSE '' END) AS logo,
	
    		(CASE WHEN images!='' THEN CONCAT('" . $url2 . "',images) ELSE '' END) AS images 
    		
    		FROM wstm_evaluate WHERE status = 1 ORDER BY orders ASC LIMIT 6";
    
    		$query = $this->db->query($sql);
    		
    		$data->list = $query->result_object();
		}	
		
		$this->responsesuccess($this->lang->line('success'), $data);
	}
    
    public function getGroupProductHome(){
        
        $url = base_url() . 'public/pages/';

        $sql = "SELECT t1.id, t1.name, t1.link , t2.link as parent_link,
        
        (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images
		
		FROM wstm_page AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.status = 1 AND t1.type = 3  AND t1.pin = 1 ORDER BY t1.orders ASC";

        $query = $this->db->query($sql);

        $data = $query->result_object();		

        $this->responsesuccess($this->lang->line('success'), $data);
    }

    public function getAboutUs()
    {
        $url = base_url() . 'public/pages/';

        $sql = "SELECT name,  link, description,title, link_video,

		(CASE WHEN images!='' THEN CONCAT('" . $url . "', images) ELSE '' END) AS images 
	
		FROM wstm_page 
		
		WHERE status = 1 AND id = 2 ";

        $query = $this->db->query($sql);

        $data = $query->row_object();

        $this->responsesuccess($this->lang->line('success'), $data);
    }

	public function getServiceFooter(){

        $url = base_url() . 'public/pages/';

        $sql = "SELECT  name, link,description, (CASE WHEN images !='' THEN CONCAT('" . $url . "', images) ELSE '' END) as images
		
		FROM wstm_page 
		
		WHERE status = 1 AND type = 2 AND parent_id = 23";

        $query = $this->db->query($sql);

        $data = $query->result_object();
		
        $this->responsesuccess($this->lang->line('success'), $data);
    }
    
    public function contentHome()
    {
        $url = base_url() . 'public/pages/';

        $sql = "SELECT id, link, name , description,  title,
		
		(CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images 
		
		FROM wstm_page WHERE id = 123 AND status = 1";

        $query = $this->db->query($sql);

        $data = $query->row_object();

        if ($data != null && $data->id > 0) {
            
            $url = base_url() . 'public/contents/';

            $sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , 
		
				(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
				
				t2.name as page_name,  CONCAT(t3.link, '/', t2.link) AS parent_link
				
				FROM wstm_content as t1 
				
				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
			    LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
									
				WHERE t1.status = 1 AND t1.pin = 1 ORDER BY t1.maker_date DESC ";

            $query = $this->db->query($sql);

            $data->list = $query->result_object();
        }

        $this->responsesuccess($this->lang->line('success'), $data);
    }
	/**
     * 
     * Create
     * Name: NGUYỄN NGỌC TOÀN   
     * Date : 25/01/2021
     * Note: API PAGE PRODUCT / PRODUCT DETAIL
     * ------------------
     * Edit
     * Name:  
     * Date : 
     * Note : 
     *
     **/
	public function getProduct(){

        $url = base_url() . 'public/products/';
		
		$link = isset($this->params['link']) ? $this->params['link'] : '';
		
		$sql = "SELECT t1.id, t1.name, t1.link, t2.name as parent_name, t1.parent_id, t2.link as parent_link
			
 		FROM wstm_page AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.status = 1 AND t1.type = 3";

		$sql .= " AND t1.link = '".$link."'" ;
		
		$query = $this->db->query($sql);
	
		$data = $query->row_object();
		
		if(!empty($data) AND $data->id > 0 ){
		   								
		    $sql = "SELECT t1.name, t1.link, (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		
			t1.price, t1.price_sale, t1.percent, t1.page_id, t2.link as parent_link, t1.views, t1.maker_date
		
			FROM pdtb_product AS t1 
		
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
			
			LEFT JOIN wstm_page AS t3 ON t3.id = t2.parent_id
		
			WHERE t1.status = 1";
			
			if( $data->parent_id > 0) {

				$sql .= " AND (t1.page_id = ".$data->id." OR t2.parent_id=".$data->id." OR t3.parent_id = ".$data->id.")";
			}

			$sql.=" ORDER BY t1.maker_date DESC ";

    		$query = $this->db->query($sql);
    						
    		$data->list = $query->result_object();
    		
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
	
	}

	public function getCategory(){

        $url = base_url() . 'public/pages/';
		
        $sql = "SELECT t1.id, t1.name, t1.link, t2.link as parent_link,
		
		(CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images
		
		FROM wstm_page AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.status = 1 AND t1.type = 3 AND t2.link = 'khoa-hoc' ORDER BY t1.orders ASC";

        $query = $this->db->query($sql);

        $data = $query->result_object();
		
		if(!empty($data)){
			
			foreach ($data as $value) {
			
				$sql = "SELECT id, name, link
				
				FROM wstm_page WHERE status = 1 AND parent_id = ".$value->id. " ORDER BY orders ASC";
				
				$query = $this->db->query($sql);
				
				$childCats = $query->result_object();
			
				$value->list = $childCats;
			
			}
		}
        $this->responsesuccess($this->lang->line('success'), $data);
    }
	
	public function getproductdetail()
	{
		$link = $this->params['link'];

		$sql = "UPDATE pdtb_product SET  views = views + 1  where link='" . $link . "'";

		$this->db->query($sql);
		
		$url = base_url() . 'public/products/';

		$sql = "SELECT t1.* , t2.id AS page_id, t3.link as parent_link, t3.name as parent_name,
		
		(CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		
		t2.name AS page_name, t2.link AS page_link
		
		FROM pdtb_product AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		LEFT JOIN wstm_page AS t3 ON t3.id = t2.parent_id
		
		WHERE t1.link='" . $link . "' AND t1.status = 1";

		$query = $this->db->query($sql);
		
		$data = $query->row_object();

		if(!empty($data) && $data->page_id > 0){
		
			$sql = "SELECT t1.name, t1.price, t1.price_sale , t1.percent, t1.link, t1.views, t2.link AS parent_link,
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images
			
			FROM pdtb_product AS t1
		
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
			
			WHERE t1.page_id=" . $data->page_id . " AND t1.id != ". $data->id." AND t1.status = 1 LIMIT 8";
			
			$query = $this->db->query($sql);

			$data->related = $query->result_object();
			
		}
				
		$this->responsesuccess($this->lang->line('success'), $data);
	}	

	public function listAttribute($id)
	{
		
		$sql = " SELECT t1.id, t1.name, t2.value, t2.attribute_id FROM pdtm_attribute AS t1
		
		LEFT JOIN pdtb_product_att AS t2 ON t1.id = t2.attribute_id		
		
		WHERE t1.status =1 AND t2.product_id = ".$id;
		
		$list = $this->db->query($sql)->result_object();
		
		return $list;
	}

	public function listPriceAttribute($id)
	{
		
		$sql = " SELECT t1.id, t1.product_id, t1.price, t1.att_id_value, t1.images, t1.status FROM pdtb_product_price_att AS t1
				
		WHERE t1.status = 1 AND t1.product_id = ".$id;
		
		$list = $this->db->query($sql)->result_object();

		if (count($list) > 0) {

			foreach ($list as $key => $value) {

				$listid = ($value->att_id_value && strlen($value->att_id_value) > 1) ? json_decode($value->att_id_value) : array();

		        if (count($listid) > 0) {

					$sql = " SELECT t1.id AS attribute_id, t1.name, t2.name AS parent_name, t1.parent_id FROM pdtm_attribute AS t1	

					LEFT JOIN pdtm_attribute AS t2 ON t1.parent_id = t2.id			
		           
					WHERE t1.id IN (" . implode(',', $listid) . ") ";

		            $query = $this->db->query($sql);

		            $list[$key]->list = $query->result_object();
		        }
			};
		}
		
		return $list;
	}

  	public function serviceList() {

    	$url = base_url() . 'public/pages/';

        $sql = "SELECT name, link FROM wstm_page

    			WHERE id = 102 AND status = 1";

 		$query = $this->db->query($sql);

        $data = $query->row_object();

        
        $message = $this->lang->line('success');

        $this->responsesuccess($message, $data);
    }
  	
  // api getListDocument
    public function getListDocument()
	{
    	$url = '/public/document/';

		$sql = "SELECT t1.id , t1.name, t1.title, t1.detail, t1.description, t1.link
        
		FROM wstm_page AS t1
	
		WHERE t1.id = 46 AND t1.status = 1";

		$query = $this->db->query($sql);	
		
		$data = $query->row_object();
		
		if(!empty($data) && $data->id){
			
			$sql = "SELECT *, (CASE WHEN file !='' THEN CONCAT('" . $url . "', file) ELSE '' END) AS file FROM tb_document WHERE status = 1 ORDER BY maker_date DESC ";

			$query = $this->db->query($sql);	
			
			$data->list = $query->result_object();
			
			$sql = "SELECT id, name, parent_id FROM tb_group_document WHERE status = 1 ORDER BY maker_date DESC ";
			
			$query = $this->db->query($sql);	
			
			$data->group = $query->result_object();
			
			$sql = "SELECT id, name FROM tb_type_document WHERE status = 1 ORDER BY maker_date DESC ";
			
			$query = $this->db->query($sql);	
			
			$data->type = $query->result_object();
			
			$sql = "SELECT id, name FROM tb_field WHERE status = 1 ORDER BY maker_date DESC ";
			
			$query = $this->db->query($sql);	
			
			$data->field = $query->result_object();
			
			$sql = "SELECT id, name FROM tb_agencies WHERE status = 1 ORDER BY maker_date DESC ";
			
			$query = $this->db->query($sql);	
			
			$data->agencies = $query->result_object();
			
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
	}

	// 	getSearchDocument
    public function getSearchDocument(){

		$option = isset($this->params['value']) ? $this->params['value']:'';
		
		$idGroup = isset($this->params['idGroup']) ? $this->params['idGroup'] : 0;
		
		$idField = isset($this->params['idField']) ? $this->params['idField'] : 0;
		
		$idType = isset($this->params['idType']) ? $this->params['idType'] : 0;
		
		$idAgencies = isset($this->params['idAgencies']) ? $this->params['idAgencies'] : 0;

		$option = urldecode($option);
		
		$url =  '/public/document/';
		
		$sql = "SELECT t1.id , t1.name, t1.title,  t1.description, t1.link
        
		FROM wstm_page  AS t1
	
		WHERE t1.id = 46 AND t1.status=1";
		
		$query = $this->db->query($sql);	
		
		$data = $query->row_object();
		
		if(!empty($data) && $data->id){
			    
			$sql = "SELECT *, (CASE WHEN file !='' THEN CONCAT('" . $url . "', file) ELSE '' END) AS file 
			
			FROM tb_document WHERE status = 1 "; 
			
			if($idGroup > 0){
			    
			    $sql .= " AND group_document_id =".$idGroup." ";
			}
			if($idField > 0){
			    
			    $sql .= " AND field_id =".$idField." ";
			}
			if($idType > 0){
			    
			    $sql .= " AND type_document_id =".$idType." ";
			}
			if($idAgencies > 0){
			    
			    $sql .= " AND agencies_id =".$idAgencies." ";
			}
			
			if($option != ''){
				
				$sql .=" AND ( ".$this->processKeySearch('name',$option)." )  ORDER BY (name LIKE '%".$option."%' ) ";
				
			}else{
				
				$sql .=" ORDER BY maker_date DESC";
				
			}

			$query = $this->db->query($sql);	
			
			$data->list = $query->result_object();
			
		}
      
        $this->responsesuccess($this->lang->line('success'), $data);
	}


	// getNotification
	public function getNotification()
	{
		$limit = isset($this->params['limit']) ? $this->params['limit'] : 0;	
		
		$sql = "SELECT t1.id , t1.name, t1.title, t1.description, t1.link,
		
		t2.name AS parent_name, t2.link AS parent_link
		
		FROM wstm_page  AS t1
		
		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id
		
		WHERE t1.id = 38 AND t1.status=1";
		
		$query = $this->db->query($sql);	
		
		$data = $query->row_object();
		
		if(!empty($data) && $data->id){
			
			$url = base_url() . 'public/contents/';
			
			$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , 
		
				(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,
				
				t2.name as name_group,  CONCAT(t3.link, '/', t2.link) AS parent_link
				
				FROM wstm_content as t1 
				
				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
			    LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
			
			WHERE  t1.status=1 AND t1.page_id = ".$data->id."
			
			ORDER BY t1.maker_date DESC ";

			if($limit > 0){
				
				$sql .= " LIMIT ".$limit ; 
			}
			
			$query = $this->db->query($sql);	
			
			$data->list = $query->result_object();
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
	}
  
	/**================== END API PRODUCT ==================**/
	
	
	
	function createLink($str)
	{
		$unicode = array(
			'a' => 'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ',
			'd' => 'đ',
			'e' => 'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ',
			'i' => 'í|ì|ỉ|ĩ|ị',
			'o' => 'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ',
			'u' => 'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự',
			'y' => 'ý|ỳ|ỷ|ỹ|ỵ',
			'A' => 'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
			'D' => 'Đ',
			'E' => 'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
			'I' => 'Í|Ì|Ỉ|Ĩ|Ị',
			'O' => 'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ',
			'U' => 'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự',
			'Y' => 'Ý|Ỳ|Ỷ|Ỹ|Ỵ',
		);

		foreach ($unicode as $nonUnicode => $uni) {
			$str = preg_replace("/($uni)/i", $nonUnicode, $str);
		}

		$str = $str;
		$str = strtolower(preg_replace('/[^a-zA-Z0-9\ ]/', '', $str));
		$str = preg_replace('/\s\s+/', ' ', trim($str));
		$str = str_replace(" ", "-", $str);

		return $str;
	}

	/**
	 * 
	 * Create
	 * Name: NGUYỄN NGỌC TOÀN
	 * Date: 26/01/2021
	 * Note: addcontact
	 * ------------------
	 * Edit:
	 * Name:
	 * Date:
	 * Note:
	 *
	 **/
    public function addContact() 
    {

        $data = $this->getdata();
        
        $is = false;

        if (!empty($data)) {

            $data['maker_date'] = date('Y-m-d H:i:s');

            $is = $this->db->insert('wstb_contact', $data);
        }
        // if ($is == true) {
        //     $message = '';
        //     $message .= '----------------------------------------------';
        //     $message .= "<h3>Thông tin </h3>";
        //     $message .= "<p>Họ và tên: " . $data['name'] . "</p>";
        //     $message .= "<p>Số điện thoại: " . $data['phone'] . "</p>";
        //     $message .= "<p>Email: " . $data['email'] . "</p>";
        //     $message .= "<p>Nội dung: " . $data['message'] . "</p>";
        //     $subject = $data['subject'];
        //     $mail = $data['email'];
        //     $html = 'Cảm ơn bạn đã quan tâm đến chúng tôi. Chúng tôi sẽ liên hệ đến bạn sớm nhất.';
        //     $html .= "" . $message;
        //    	$this->sendmail($mail, $subject, $html); //send email khách hàng
            
        //     $detail = '';
        //     $detail .= "<p>Hệ thống vừa nhận được tin nhắn của khách hàng.</p>";
        //     $detail .= "" . $message;
        //    	$skip = $this->sendmail('info@homeq.vn', $subject, $detail);
        // }
        if ($is == true) {

            $this->responsesuccess($this->lang->line('success'));

        } else {

            $this->responsefailure($this->lang->line('failure'));
        }
    }

	/**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN   
     * Date : 05/01/2021
     * Note: API PAGE CONTENT / CONTENT DETAIL
     * ------------------
     * Edit
     * Name:  
     * Date : 
     * Note : 
     *
     **/

    public function contentList()
    {

        $url = base_url() . 'public/contents/';

        $link = isset($this->params['link']) ? $this->params['link'] : '';

        $sql = "SELECT id, link, description, parent_id, name, keywords
		
		FROM wstm_page WHERE status = 1 AND type = 4 AND link = '" . $link . "'";

        $query = $this->db->query($sql);

        $data = $query->row_object();
        
        // CONCAT(t3.link, '/', t2.link)

        if (!empty($data)) {

            $sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.maker_date , t1.page_id , 
		
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images , t1.source,
			
			t2.name as parent_name, t2.link AS parent_link, t3.link AS parent_links
			
			FROM wstm_content as t1 
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
			
			LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
								
			WHERE  t1.status = 1 AND (t1.page_id = " . $data->id . " OR t2.parent_id = " . $data->id . " ) AND t2.type = 4 

			ORDER BY t1.maker_date DESC";

            $query = $this->db->query($sql);

            $data->list = $query->result_object();

            $sql = "SELECT t1.id, t1.name, IF(t1.orders > 0 ,t1.orders, t1.id) AS ordersby

    		FROM wstm_page AS t1 LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id 

			WHERE t1.parent_id = ".($data->parent_id > 0 ? $data->parent_id : $data->id)." AND t1.status = 1 ORDER BY ordersby ASC Limit 4";

			$query = $this->db->query($sql);

	        $data->group = $query->result_object();
        } 

        $message = $this->lang->line('success');

        $this->responsesuccess($message, $data);
    }

    public function getContentListGroup() {
	
		$id = (isset($this->params['id'])) ? $this->params['id'] : 0;

		$limit = (isset($this->params['limit'])) ? $this->params['limit'] : 0;

		$url = base_url() . 'public/contents/';
		
		// t3.name as parent_names, t3.link AS parent_links,
		
		

		$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.maker_date, t1.page_id, t1.source,

				t2.name as parent_name, CONCAT(t3.link, '/', t2.link) AS parent_link,
			
		  		(CASE WHEN t1.images != '' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images

				FROM wstm_content as t1

				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
				LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
				
				WHERE t1.page_id IN (SELECT id FROM wstm_page WHERE id = " . $id . ") AND t1.status = 1
				
				LIMIT " . $limit;

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

    public function getNewsAsidebarRight() {
        
        $limit = (isset($this->params['limit'])) ? $this->params['limit'] : 0;
        
        $type = (isset($this->params['type'])) ? $this->params['type'] : '';
        
		$url = base_url() . 'public/contents/';
		
		$sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link, t1.maker_date,  

				t2.name as parent_name, CONCAT(t3.link, '/', t2.link) AS parent_link,
			
		  		(CASE WHEN t1.images != '' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images

				FROM wstm_content as t1

				LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
				
				LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id
				
				WHERE t1.status = 1 ";
				
		if($type =='view'){
		    
		    $sql .= " ORDER BY t1.views DESC ";
		    
		}else{
		    
		     $sql .= " ORDER BY t1.maker_date DESC ";
		}
				
			$sql .= " LIMIT " . $limit;
			

        $query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
        
    }

    public function getContentDetail()
    {
        $link = isset($this->params['link']) ? $this->params['link'] : '';

        $url = base_url() . 'public/contents/';

        $url_file = base_url() . 'public/file/';

        $sql = "UPDATE wstm_content SET  views = views + 1  WHERE link = '" . $link . "'";

        $this->db->query($sql);

        $sql = "SELECT t1.id, t1.name, t1.description, t1.views, t1.link, t1.author,
		
		t2.name as parent_name, t2.link as parent_link, t3.link as parent_links, t3.name as parent_names, t1.page_id, t1.detail, t1.maker_date, t1.file AS name_file, t1.source, t1.keywords,

		(CASE WHEN t1.file!='' THEN CONCAT('" . $url_file . "', t1.file) ELSE '' END) AS link_file,  
		
		(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images 
				
		FROM wstm_content as t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id 
		
		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id 
		
		WHERE t1.link='" . $link . "' AND t1.status=1";

        $query = $this->db->query($sql);

        $data = $query->row_object();

        if (!empty($data) && $data->id > 0) {

            $url = base_url() . 'public/contents/';

            $sql = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , t1.page_id , 
			
			(CASE WHEN t1.images!='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) AS images ,	
			
			t2.name as parent_name, t2.link as parent_link
			
			FROM wstm_content as t1 
			
			LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
			
			LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id		
			
			WHERE  t1.status = 1 AND t1.page_id = " . $data->page_id . " AND t1.id != " . $data->id . " 
			
			ORDER BY t1.maker_date DESC LIMIT 6";

            $query = $this->db->query($sql);

            $data->related = $query->result_object();
        }

        $this->responsesuccess($this->lang->line('success'), $data);
    }

    /**================== END API PAGE CONTENT ==================**/

    public function getLanguages()
    {

		$url = base_url() . 'public/language/';

		$sql = "SELECT id, code, name, (CASE WHEN icon != '' THEN CONCAT('" . $url . "', icon) ELSE '' END) AS icon from `tb_language`

    		WHERE code = 'vi'";

		$default = $this->db->query($sql)->row_object();

    	$sql = "SELECT id, code, name, (CASE WHEN icon != '' THEN CONCAT('" . $url . "', icon) ELSE '' END) AS icon from `tb_language`

    		WHERE status = 1";

		$query = $this->db->query($sql);

		$default->list = $query->result_object();

		$this->responsesuccess(null, $default);
    }

    public function getHeaderInfo()
    {

        $sql = "SELECT id, text_key, title, value
        
		        FROM wstm_setting WHERE group_setting = 2 AND type = 1 AND text_key = 'FEPhone' OR text_key = 'FEMail' AND status = 1";

        $query = $this->db->query($sql);

        $list = $query->result_object();

        $list = ($list != null) ? $list :  array();

        $message = $this->lang->line('success');

        $this->responsesuccess($message, $list);
    }
}
