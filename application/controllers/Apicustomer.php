<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apicustomer extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
	}
	/**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN   
     * Date : 05/01/2021
     * Note: API CART
     * ------------------
     * Edit
     * Name:  
     * Date : 
     * Note : 
     *
     **/
     public function paymentCart()
     {

          $url = base_url() . 'public/pages/';

          $link = isset($this->params['link']) ? $this->params['link'] : '';

          $sql = "SELECT id, link, description, name , detail , title, 

          (CASE WHEN images!='' THEN CONCAT('" . $url . "',images) ELSE '' END) AS images, related

          FROM wstm_page WHERE parent_id = 28 AND status=1";

          $query = $this->db->query($sql);

          $data = $query->result_object();    
          
          $this->responsesuccess($this->lang->line('success'), $data);
    }
     //addcart
     public function addcart()
     {
         
          $data=$this->getdata();
     
          $is=false;
          
          $isDCart=false;
          
          $id_cart = isset($id_cart) ? $id_cart : 0;
          
          $data['maker_date']= date('Y-m-d H:i:s');
          
          if($data != null){

               if(array_key_exists('cart',$data)){
              
                    $cart = $data['cart'];
                    
                    if(array_key_exists('id',$cart)){  

                         $cart = array_diff_key($cart, array('id' => $cart['id']));
                         
                    }
                    if($id_cart == 0){

                         $cart['code']= time();
                         
                         $is = $this->db->insert('ortb_order', $cart);
                         
                         $id_cart = $is == true ? $this->db->insert_id() : 0;
                    }
               }
               if($id_cart > 0 && array_key_exists('cartdetail',$data)){
                    
                    $detail=$data['cartdetail'];

                    for($i = 0 ;$i < count($detail);$i++){

                         $detail[$i]['order_id'] = $id_cart;
                      
                       	 $detail[$i]['customer_id'] = $cart['customer_id'];

                         $detail[$i]['maker_date'] = date('Y-m-d H:i:s');

                         unset($detail[$i]['images']);
                    }
                    $is = $this->db->insert_batch('ortb_order_detail', $detail);
               }

          }

          if($is==true){     

               if(array_key_exists('item',$data)){

                    $item=$data['item'];
                 
                 	$this->db->where('id', $item['id']);
                 
					$is = $this->db->update('tb_student',array('phone'=>  $item['phone'], 'address'=>  $item['address']));
                 	
                    if($is == true){
                         $this->session->set_userdata('customer',true);
                         $this->session->set_userdata('customer_id',$item['id']);
                         $this->session->set_userdata('customer_name',$item['name']);
                    }
                 	
               }
               $tableCart = '';
               foreach ($item['data'] as $key => $v){
                    $tableCart.="<tr>
                         <td style='padding:10px;border: 1px solid #dddddd;'>".$v["name"]."</td>
                         <td style='padding:10px;border: 1px solid #dddddd;'>".$v["amount"]."</td>
                         <td style='padding:10px;border: 1px solid #dddddd;'>".$v["price"]."</td>
                      </tr>
                    ";
               }
               $tableCart.="</table>";
              
                //SEND EMAIL KHÁCH HÀNG
               $html ="<p>CẢM ƠN BẠN ĐÃ ĐẶT HÀNG TẠI HOMEQ.VN. CHÚNG TÔI SẼ KIỂM TRA VÀ TRẢ LỜI LẠI pẠN SỚM NHẤTT</p>";
               $html .="<h3>Thông tin người đặt hàng: </h3>";
               $html .="<p>Họ và Tên: ".$item['name']."</p>";
               $html .="<p>Phone: ".$cart['phone']."</p>";
               $html .="<p>Email: ".$item['email']."</p>";
               $html .="<p>Địa chỉ giao hàng: ".$cart['delivery_address']."</p>";
               $html .="<p>Note: ".$cart['note']."</p>";
               $html .="<h3>Thông tin chi tiết đơn hàng: </h3>";
               $html .= $tableCart;
              
                @$this->sendmail($item['email'],"ĐƠN HÀNG HOMEQ.VN",$html);
               //SEND EMAIL QUẢN TRỊ
               $html ='<p>Thông tin đơn hàng hệ thống nhận được</p> </br>';
               $html .="<h3>Thông tin người đặt hàng: </h3></br>";
               $html .="<p>Họ và Tên: ".$item['name']."</p></br>";
               $html .="<p>Phone: ".$cart['phone']."</p></br>";
               $html .="<p>Email: ".$item['email']."</p></br>";
               $html .="<p>Địa chỉ giao hàng: ".$cart['delivery_address']."</p></br>";
               $html .="<p>Note: ".$cart['note']."</p></br>";
               $html .="<h3>Thông tin chi tiết đơn hàng: </h3></br>";
               $html .= $tableCart;
               @$this->sendmail('info@homeq.vn', "Hệ thống nhận được đơn hàng",$html);
               
               $message='Bạn đã đặt hàng thành công. Nhân viên sẽ xác nhận đơn hàng của bạn trong 24h. Cảm ơn bạn';
               $this->responsesuccess($message, array('code' => $cart['code'], 'customer' => $item));
          }
          else{
               $this->responsefailure($this->lang->line('failure'));
          }              
     }
     
     /**
     * 
     * Create
     * Name: NGUYỄN VĂN TIÊN   
     * Date: 05/01/2021
     * Note: API CUSTOMER
     * ------------------
     * Edit
     * Name:  
     * Date: 
     * Note: 
     *
     **/
     public function logout()
    {

        @$this->session->sess_destroy();

        $this->responsesuccess(null, array());
    }

     public function loginCustomer() {
          
          $data=$this->getdata();

          $is= false;
          
          $list = array();
          
          $password = isset($data['password']) ? sha1($data['password']) : '';

          $code = isset($data['code']) ? $data['code'] : '';
          
          $sql = "SELECT * FROM tb_student WHERE status = 1 AND code='" . $code . "' AND password ='" . $password . "' ";
          
          $query = $this->db->query($sql);

          $list = $query->row_object();
          
          if(!empty($list)) {
               
               $this->session->set_userdata('customer',true);

               $this->session->set_userdata('customer_id',$list->id);
               
               $this->session->set_userdata('customer_name',$list->name);
               
               $is = true;
          }
               
          $is == true ? $this->responsesuccess($this->lang->line('loginSuccess'), $list) : $this->responsefailure($this->lang->line('failLoginStudent'));
     }

     public function checkcustomer()
     {

          $data = $this->getdata();
          
          $list = null;

          $skip = false;

          if ($data !== null) {

               $password = isset($data['password']) ? $data['password'] : '';

               $email = isset($data['email']) ? $data['email'] : '';

               $sql = "SELECT * FROM tb_student WHERE email = '".$email."' AND status = 1 AND password = '".$password."' ";

               $query = $this->db->query($sql);

               $list = $this->db->query($sql)->row_object();

               if (!empty($list)) {
                     
                    $skip = true;
                     
                    $this->session->set_userdata('customer', true);
                     
                    $this->session->set_userdata('customer_id', $list->id);
                     
                    $this->session->set_userdata('customer_name', $list->name);
               }
          }

          $list = array('skip' => $skip, 'data' => $list);
        
          echo json_encode($list);
     }



     public function registration() {

          $data = $this->getdata();

          $is = false;

          $message = $this->lang->line('failure');

          if(!empty($data)) {

               $data['maker_id'] = $this->session->userdata('user_id');

               $data['maker_date'] = date('Y-m-d H:i:s');

               $data['status'] = 1;
            
               $data['password'] = array_key_exists('password', $data) ?  sha1($data['password']) : '';

               $email = array_key_exists('email', $data) ?  $data['email'] : '';
          	
               $password = array_key_exists('password', $data) ?  $data['password'] : '';
               
               $sql = "SELECT count(id) as count FROM `tb_student` WHERE email= '".$email."' ";

               if($this->db->query($sql)->row_object()->count == 0) {

                    $is = $this->db->insert("tb_student", $data);

                    $message = ($is == true) ? $this->lang->line('success') : $message;

               } else {

                    $message = 'Email của bạn đã tồn tại trong hệ thống.';
               }
          }

          if($is) {

               $sql = "SELECT * FROM tb_student WHERE status = 1 AND email='" . $email . "' AND password ='" . $password . "'";
          
               $query = $this->db->query($sql);
               
               $list = $query->row_object();
               
               if(!empty($list)){
               
                    $this->session->set_userdata('customer',true);
                    $this->session->set_userdata('customer_id',$list->id);
                    $this->session->set_userdata('customer_name',$list->name);
               }

               $this->responsesuccess($message,$list);

          } else {

            $this->responsefailure($message);
          }
     }

     public function infoCustomer()
     {    
          $id = $this->params['id'];
          
          $sql="SELECT id, name, email, phone, address, sex FROM tb_student WHERE id=".$id;
          
          $query = $this->db->query($sql);
          
          $list = $query->row_object();
          
          $this->responsesuccess($this->lang->line('success') , $list);
     }

     public function updateCustomer()
     {    
          $data = $this->getdata();
          
          $is = false;
          
          if($data['id'] && $data['id'] > 0){
               
               $sql="SELECT count(id) as count FROM tb_student WHERE phone='".$data['phone']."' and id != ".$data['id'];
               
               if($this->db->query($sql)->row_object()->count == 0){
                    
                    $this->db->where('id',$data['id']);

                    if (array_key_exists('avatar', $data)) {

                         $data['avatar'] = $this->processimages($data['avatar']);
                         
                    } else {

                         $data['avatar'] = '';
                    }
                    
                    $is = $this->db->update("tb_student", $data);
                    
                    $is = ($is==true) ? 1 : 0;
                    
                    $sql="SELECT * FROM tb_student WHERE id=".$data['id'];
     
                    $query = $this->db->query($sql);
                    
                    $list = $query->row_object();
                    
                    if(!empty($list)){
                         $this->session->set_userdata('customer',true);
                         $this->session->set_userdata('customer_id',$list->id);
                         $this->session->set_userdata('customer_name',$list->name);
                         $is = true;
                    }
               }
               else{
                    $this->responsefailure($this->lang->line('checkExitPhone'));
               }
               
          }
     
          $is == true ?  $this->responsesuccess($this->lang->line('success'), $list) :$this->responsefailure($this->lang->line('failure'));   
     }
     
     public function resetPassword()
     {
          $data = $this->getdata();
          
          $is =false;
     
          if(!empty($data)){
               
               $sql="SELECT count(id) as count, id FROM tb_student where email='".$data['email']."' GROUP BY id ";
               
               $id = isset($this->db->query($sql)->row_object()->id) ? $this->db->query($sql)->row_object()->id : 0;
               
               if( $id > 0){
                    
                    $data['token']=sha1(time());
                    $this->db->where('id',$id);
                    $is = $this->db->update('tb_student', $data);
               }
               if($is == true){
                   
                    $message = "<!DOCTYPE html>
    				<html lang='vi'>
    				<head>
    					 <meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
    				</head>
    				<body>
    					<div >
                           <p>Chào quý khách!</p>
                           <p>  Chúng tôi nhận được yêu cầu thay đổi mật khẩu của quý khách.</p>
                           <p>  Xin hãy click vào đường dẫn sau để đổi mật khẩu: ". base_url() ."quen-mat-khau/".$data['token']."</p>
                           <p>  Mọi thắc mắc và góp ý vui lòng liên hệ với chúng tôi qua email: info@homeq.vn </p>
                           <p>  Trân trọng, </p>
                        </div>  
    				</body >
    				</html >";
                    $email = $data['email'];
                    
                    // @$this->sendmail($email,"Thay Đổi Mật khẩu",$message);
                    
                    $this->responsesuccess('Quá trình của bạn thành công. Bạn hãy kiểm tra lại email.');
               }
          }
          $this->responsefailure('Mail của bạn chưa đăng ký tài khoản');
     }
     
     public function checkTokenResetPassword ()
     {
          
          $is = false;
          
          $sql = "SELECT id FROM tb_student WHERE token = '".$this->params['value_token']."' ";
         
          $data = $this->db->query($sql)->row_object();
    
          $is =  ($data && $data->id > 0) ? 1 : 0;
          
          $is == true ?  $this->responsesuccess($this->lang->line('success'),$data) :$this->responsefailure($this->lang->line('failure'));  

     }

     public function companySignin()
     {

          $code = isset($this->params['code']) ? $this->params['code'] : '';

          if($code != '') {

               $sql = "SELECT count(code_company) as count FROM tb_student WHERE code_company = '".$code."'";

               if($this->db->query($sql)->row_object()->count >= 1) {


                    $this->responsesuccess($this->lang->line('loginSuccess'), $code);

               } else {

                    $this->responsefailure($this->lang->line('codeCompanyNotExist'));
               }

          } 
     }


     public function getAllStudentByCompany()
     {

          $code = isset($this->params['code']) ? $this->params['code'] : '';

          if($code != '') {

               $sql = "SELECT t1.id AS student_id,t1.code AS student_code,t1.name AS student_name, t2.score_avg, t3.name AS product_name, t3.id AS product_id, t4.name AS class_name

                         FROM tb_student AS t1 

                         LEFT JOIN tb_student_product AS t2 ON t1.id = t2.student_id 

                         LEFT JOIN pdtb_product AS t3 ON t2.product_id = t3.id 

                         LEFT JOIN tb_class AS t4 ON t2.class_id = t4.id 

                         WHERE t1.code_company = '" . $code . "' AND t1.status = 1";
               
               $data = $this->db->query($sql)->result_object();

               $this->responsesuccess($this->lang->line('success'), $data);

          }

           $this->responsefailure($this->lang->line('failure'));

     }

     /*
     public function getAllStudentByCompany()
     {

          $code = isset($this->params['code']) ? $this->params['code'] : '';

          if($code != '') {

               $sql = "SELECT tb_1.code_company, tb_1.student_id, tb_1.student_code, tb_1.student_name, GROUP_CONCAT(tb_1.name SEPARATOR ', ') AS class_name
                    
                         FROM (SELECT t1.code_company, t1.id AS student_id, t1.name AS student_name, t3.name FROM tb_student AS t1
                   
                         LEFT JOIN tb_student_product AS t2 ON t1.id = t2.student_id
              
                         LEFT JOIN tb_class AS t3 ON t2.class_id = t3.id
                         
                         WHERE t1.code_company = '" . $code . "' AND t1.status = 1
                         
                         GROUP BY t3.name, t1.id

                         ORDER BY t1.maker_date DESC) AS tb_1 GROUP BY tb_1.student_id";
               
               $data = $this->db->query($sql)->result_object();

               $this->responsesuccess($this->lang->line('success'), $data);

          }

           $this->responsefailure($this->lang->line('failure'));

     }
     */
     
     public function changePasswordCustomer()
     {
          $is = false;
          
          $data = $this->getdata();
          
          $id = isset($this->params['id']) ? $this->params['id'] : 0;
       
       	  $resetPassword = isset($this->params['type']) ? $this->params['type'] : 0;
          
          if($id > 0){
               
               if($resetPassword != 1){
                 
                   $password = array_key_exists('password',$data) ? SHA1($data['password']) : '' ; 

                   $sql = "SELECT count(id) as count FROM tb_student WHERE password = '".  $password."' ";

                   if ($this->db->query($sql)->row_object()->count == 1) {

                        $this->db->where('id', $id);

                        $is = $this->db->update('tb_student', array('password'=> sha1($data['password_new'])));

                        $is = ($is==true) ? 1 : 0;
                   }
               }else{
                   
                   $this->db->where('id', $id);

                   $is = $this->db->update('tb_student', array('password'=> sha1($data['password']), 'token' => sha1(time())));

                   $is = ($is==true) ? 1 : 0;
               }
          }
          $is == true ? $this->responsesuccess($this->lang->line('success')) : $this->responsefailure($this->lang->line('failurePasswordOld'));  

     }
  	
  	public function loginfacegoogle(){
		
		$data = $this->getdata();
		$is = false;
		$sql="SELECT count(id) as count  FROM tb_student WHERE email='".$data['email']."' ";

		if($data != null){
			$data['maker_date']= date('Y-m-d H:i:s');
			$data['code']=time();	
			if(array_key_exists('password',$data)){
				$data['password'] = SHA1($data['password']);
			}
	
			if($this->db->query($sql)->row_object()->count == 0){
			
				$is=$this->db->insert('tb_student',$data);
				
			}

			$sql ="SELECT *  FROM tb_student WHERE email='".$data['email']."' AND id_type_social='".$data['id_type_social']."' AND type_user='".$data['type_user']."'";
			$query = $this->db->query($sql);
    		$list = $query->row_object();
    		
			if($list){
    			$this->session->set_userdata('customer',true);
    			$this->session->set_userdata('customer_id',$list->id);
    			$this->session->set_userdata('customer_name',$list->name);
    			$is = true;
			}

			if($is){
				$this->responsesuccess($this->lang->line('loginSuccess') , $list);
			}else{
				$this->responsefailure($this->lang->line('checkExitEmail'));
			}
		}
	}
  
  
     public function getListProducts(){
          
          $id = $this->params['id'];
          
          $sql = "SELECT t1.code AS order_code, t4.name AS service_name, t2.guarantee, t2.maker_date, t2.id AS order_detail_id, t2.amount , t3.name, COUNT(t5.id) AS count_document
          
               FROM ortb_order AS t1
               
               LEFT JOIN ortb_order_detail AS t2 ON t1.id = t2.order_id
               
               LEFT JOIN pdtb_product AS t3 ON t2.product_id = t3.id
               
               LEFT JOIN wstm_page AS t4 ON t2.service_id = t4.id AND t4.type = 6
               
               LEFT JOIN tb_document_customer AS t5 ON t2.id = t5.order_detail_id
               
               WHERE t1.customer_id = " . $id . " AND t1.delivery_status = 2 GROUP BY t2.id";

          $query = $this->db->query($sql);

          $data = $query->result_object();
          
          $this->responsesuccess($this->lang->line('success') , $data);
     }
     
     public function getProductCustomer (){
          
          $order_detail_id = $this->params['order_detail_id'];
          
          $id = $this->params['id'];
          
          $url = base_url() . 'public/products/';
          
          $sql = "SELECT t2.name, t2.info, t2.device, t2.detail, t1.amount, t1.price, t1.guarantee,

               (CASE WHEN t2.images !='' THEN CONCAT('" . $url . "', t2.images) ELSE '' END) as images
               
               FROM ortb_order_detail AS t1

               LEFT JOIN pdtb_product AS t2 ON t1.product_id = t2.id

               WHERE t1.id = ". $order_detail_id. " AND t1.customer_id = ". $id;
                         
          $query = $this->db->query($sql);
          
          $data = $query->row_object();
          
          if(!empty( $data) ){

               $sql = "SELECT t2.id, t2.name
                    FROM tb_document_customer AS t1
                    LEFT JOIN tb_config_product_document AS t2 ON t1.document_product_id = t2.id
                    WHERE t1.order_detail_id = ". $order_detail_id;

               $query = $this->db->query($sql);
          
               $data->document_customer = $query->result_object();
          }

          $this->responsesuccess($this->lang->line('success') , $data);
     }

     public function getDocumentInfo() {

          $document_id = isset($this->params['document_id']) ? $this->params['document_id'] : 0; 

          $sql = "SELECT name, description, detail FROM tb_config_product_document WHERE status = 1 AND id = ".$document_id;

          $query = $this->db->query($sql);

          $data = $query->row_object();

          $this->responsesuccess($this->lang->line('success') , $data);    

     }
     
     public function listOrder()
     {
          
          $customer_id = $this->params['id'];
          
          $sql="SELECT t1.code, t1.day_start, t1.price_total , t1.delivery_status, t2.name 
          
          FROM ortb_order AS t1 
          
          LEFT JOIN tb_student AS t2 ON t2.id = t1.customer_id 
          
          WHERE t1.customer_id=".$customer_id;
          
          $query = $this->db->query($sql);
          
          $data = $query->result_object();
          
          $this->responsesuccess($this->lang->line('success') , $data);
     }
     
     public function getCartDetail()
     {
          
          $code = $this->params['code'];
                    
          $sql="SELECT t1.id, t1.code, t1.day_start, t1.price_total , t1.delivery_status,
          
          t1.phone,  t1.delivery_address, t1.note, t1.amount_total, t2.name 
          
          FROM ortb_order AS t1 
          
          LEFT JOIN tb_student AS t2 ON t2.id = t1.customer_id 
          
          WHERE t1.code=".$code;
          
          $query = $this->db->query($sql);
          
          $cart = $query->row_object();
          
         if(!empty( $cart) &&  $cart->id &&  $cart->id > 0 ){

               $url = base_url().'public/products/';

               $sql="SELECT t1.* , t2.name, t2.code, 

               (CASE WHEN t2.images != '' THEN CONCAT('" . $url . "', t2.images) ELSE '' END) as images, 
               
               t2.link, t3.link AS parent_link FROM ortb_order_detail AS t1 
               
               LEFT JOIN pdtb_product AS t2 ON t2.id = t1.product_id 
               
               LEFT JOIN wstm_page AS t3 ON t2.page_id= t3.id WHERE t1.order_id = ".$cart->id;
               
               $query = $this->db->query($sql);
               
               $detail = $query->result_object();
          }
          
          $data = array('cart'=> $cart,'detail'=>$detail);
          
          $this->responsesuccess($this->lang->line('success') , $data);
     }
    /**================== END API CART ==================**/

     public function getListClass() {
          $id = $this->params['id'];

          $product_id = $this->params['product_id'];

          $sql = "SELECT t1.product_id, t2.name AS product_name, t3.name AS class_name,

                    t1.is_attendance, t1.is_discipline, t1.score_factor_1, t1.score_factor_2,

                    t1.score_1, t1.score_2, t1.score_avg, t1.is_pass

                    FROM `tb_student_product` AS t1 

                    LEFT JOIN pdtb_product AS t2 ON t1.product_id = t2.id 

                    LEFT JOIN tb_class AS t3 ON t1.class_id = t3.id 

                    WHERE t1.student_id = ".$id." AND t1.product_id = ". $product_id;

          $data = $this->db->query($sql)->result_object();

          $this->responsesuccess($this->lang->line('success') , $data);

     }

     /*    
     public function getListClass() {

          $id = $this->params['id'];

          $sql = "SELECT t1.class_id, t2.name AS class_name, t3.score_avg AS majors_score_avg FROM `tb_student_product` AS t1 

                    LEFT JOIN tb_class AS t2 ON t1.class_id = t2.id 

                    LEFT JOIN tb_student_majors AS t3 ON t1.class_id = t3.class_id

                    WHERE t1.student_id = ".$id." GROUP BY t1.class_id";

          $data = $this->db->query($sql)->result_object();

          if(!empty($data)) {

               for($i = 0; $i < count($data); $i++ ) {

                    $sql = "SELECT t1.product_id AS product_id, t1.is_videos AS videos, t1.is_attendance, t1.is_discipline,

                            t1.score_factor_1, t1.score_factor_2, t1.score_1, t1.score_2, t1.score_avg, t1.is_pass,

                            t2.name AS product_name

                            FROM `tb_student_product` AS t1
                  
                            LEFT JOIN `pdtb_product` AS t2 ON t1.product_id = t2.id
                              
                            WHERE class_id = ".$data[$i]->class_id." AND student_id = " . $id . "
                            
                            ORDER BY t1.maker_date DESC";

                    $query = $this->db->query($sql);

                    $data[$i]->product_list = $query->result_object();

               }

               $this->responsesuccess($this->lang->line('success') , $data);

          }

          $this->responsefailure($this->lang->line('failure'));

     }
     */

     public function getCourseVideos() {

          $id = $this->params['id'];

          $sql = "SELECT videos, name FROM `pdtb_product` WHERE id = ".$id;

          $query = $this->db->query($sql);

          $data = $query->row_object();

          $this->responsesuccess($this->lang->line('success') , $data);

     }

}