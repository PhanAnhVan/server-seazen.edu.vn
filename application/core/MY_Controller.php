<?php
defined('BASEPATH') or exit('No direct script access aloowed');

class MY_Controller extends CI_Controller
{
	public $USER;

	public $typeResponse = false;

	public $token = '';

	public $ipRemote;

	public $headers = array();

	public $params = array();
	
	public $debug = true;

	function __construct()
	{
		parent::__construct();

		$this->load->database();
		
		$this->load->library('session');

        if($this->debug == true){
			
			$this->session->set_userdata('user', true);

			$this->session->set_userdata('user_id', 1);
		}

		$this->lang->load('message', $this->config->item('language'));

		$this->controller = strtolower($this->uri->segment(1));

		$uriapi = array('get', 'set');
		
		$uriresult = array('get-result', 'set');
		
		if(in_array($this->controller,$uriapi)){
			if(!$this->session->userdata('user') || !$this->session->userdata('user_id')){
				redirect($this->config->item('base_url'));die;
			}
		}	
		
		$this->request();
	}

	public function response($response)
	{

		if ($this->typeResponse == false) {

			$uri = "";

			$uri = (array_key_exists('baseResponse', $this->params)) ? $this->params['baseResponse'] . $uri : $this->config->item('base_url') . 'response' . $uri;

			redirect($uri);

		} else {

			echo json_encode($response);

			die;
		}
	}

	public function responsesuccess($message, $data = "")
	{

		$token = array_key_exists('mask', $this->params) ? $this->params['mask'] : 'tokenApi';

		$res = array('token' => $token, 'status' => 1, 'message' => $message, 'data' => ($data == null || $data == '') ? array() : $data);

		$this->response($res);
	}

	public function responsefailure($message)
	{

		$token = array_key_exists('mask', $this->params) ? $this->params['mask'] : 'tokenApi';

		$this->response(array('token' => $token, 'status' => 0, 'message' => $message, 'data' => $this->input->get()));
	}

	public function request()
	{

		$params = $this->input->get();

		if (array_key_exists('token', $params)) {

			$this->token = $params['token'];

			$params = array_diff_key($params, array('token' => $params['token']));
		}

		if (array_key_exists('typeResponse', $params)) {

			$this->typeResponse = $params['typeResponse'];

			$params = array_diff_key($params, array('typeResponse' => $params['typeResponse']));

		} else {

			$this->typeResponse = true;
		}

// 		$this->header = apache_request_headers();

		$this->params = $params;
	}

	public function getdata()
	{

		$data = $this->input->post();

		if (empty($data)) {

			$data = json_decode($this->input->raw_input_stream, TRUE);
		}
		return $data;
	}

	public function history($data)
	{

		$obj = array(

			'user_id' => $data['user_id'],

			'maker_id' => $data['user_id'],

			'type' => $data['type'],

			'maker_date' => date('Y-m-d H:i:s'),

			'tables' => $data['table'],

			'status' => $data['status']
		);

		if (array_key_exists('container', $data)) {

			$obj['container'] = (gettype($data['container']) == 'object' || gettype($data['container']) == 'array') ? json_encode($data['container']) : $data['container'];
		}

		if (array_key_exists('data', $data)) {

			$obj['data'] = (gettype($data['data']) == 'object' || gettype($data['data']) == 'array') ? json_encode($data['data']) : $data['data'];
		}

		$is = $this->db->insert('sctb_history', $obj);

		return $is;
	}

	public function processimages($data)
	{
		$local = $this->config->item('base_url');

		$add = $data['add'];

		$del = $data['del'];

		$fieldName = $data['old'];
		
		$mimeimages = $this->config->item('mimeimages');

		$path = str_replace($local, './', $data['path']);

		if (count($del) > 0) {
			
			foreach ($del as $k) {
				
				$pathold = $path . $k;
				
				if (file_exists($pathold)) {
					
					@unlink($pathold);
				}
			}
		}
		if (!is_dir($path)) {
			
			@mkdir($path, 0757);
		}
		if (count($add) > 0) {
			
			foreach ($add as $key => $value) {

				$temp = explode(".", $value['name']);

				$extension = strtolower(end($temp));

				$newname =  removesign(rtrim($value['name'], "." . $extension));

				$newname = (strlen($newname) > 200) ? substr($newname, 0, 200) : $newname;

				$namefile = $newname . '_' . time() . '.' . $extension;

				if (file_exists($path . $namefile)) {

					$newname = $newname . '_' . time() . '.' . $extension;
				} else {

					$newname = $namefile;
				};
				if($value['type'] =='pdf'){
					
						@$file = file_get_contents($value['src']);
						
                        @$result = file_put_contents($path.$newname, $file);
						
                        if($result > 0){
							
                            array_push($fieldName, $newname);
                        }

				} else {

					@$url = getimagesize($value['src']);

					if (is_array($url)) {

						if (in_array($extension, $mimeimages)) {

							@$file = file_get_contents($value['src']);

							@$result = file_put_contents($path . $newname, $file);

							if ($result > 0) {

								array_push($fieldName, $newname);
							}
						}
					}
				}
			}
		}

		$fieldName = ($data['multiple'] == true) ? json_encode($fieldName) : ((count($fieldName) > 0)  ? $fieldName[0] : "");

		return $fieldName;
	}

	public function sendmail($emailto, $subject, $message)
	{

		$this->load->library('email');

		$email = $this->config->item('config_mail');
		
		$config = array(

		    'protocol' => 'smtp',
			
			'smtp_host' => $email['local'],

			'smtp_user' => $email['mail'],

			'smtp_pass' => $email['password'],
			
			'smtp_port' => 587,
			
			'smtp_crypto' => 'tls', 
			
			'crlf' => "\r\n",
			
			'starttls' => TRUE,
			
			'validate' => false,
			
            'charset' => 'utf-8',

			'wordwrap'  => TRUE
		);
 
		$this->email->initialize($config);
		
		$this->email->set_newline("\r\n"); 

		$this->email->from('info@homeq.vn','HomeQ');

		$this->email->to($emailto);

		$this->email->subject($subject);
		
		$this->email->message($message);
		
		$this->email->set_mailtype("html");
		
		$sent = $this->email->send();
		
        if ($sent)  {
            
             return true;
             
        } else {
            
            echo 'lỗi';
            
           return false;
        } 
	}

}
