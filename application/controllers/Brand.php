<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Brand extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "prtm_brand";
	}
	public function getlist()
	{

		$url = base_url() . 'public/brand/';

		$sql = "SELECT id, name, maker_date, status FROM " . $this->table . " ORDER BY maker_date  DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = isset($id) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE name='" . $data['name'] . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);
				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('checkExitBrand');
			}
		} else {

			$message = $this->lang->line('failure');
		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	public function remove()
	{
		$id = $this->params['id'];

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "select count(id) as count from pdtb_product where brand_id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);
			} else {

				$message .= $this->lang->line('checkDeleteBrand');
			}
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
