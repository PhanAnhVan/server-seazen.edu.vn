<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order extends MY_Controller{

	function __construct(){

		$this->table = "ortb_order";

		parent:: __construct();
	}

	public function getlist(){

		$sql = " SELECT t1.id, t1.code, t1.delivery_address, t1.price_total, t1.amount_total, t1.status, t1.maker_date , t2.name as customer_name
			FROM ".$this->table." AS t1
			LEFT JOIN cstb_customer AS t2 ON t2.id = t1.customer_id
			ORDER BY t1.code DESC ";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $list);
	}

	public function getrow(){

		$id = $this->params['id'];

		$sql="SELECT code, customer_id, phone, delivery_address, delivery_status, amount_total, price_total, note, status
		FROM ortb_order
		WHERE id=".$id;

		$query = $this->db->query($sql);

		$info = $query->row_object();

		$sql="SELECT t1.id AS order_detail_id, t1.product_id AS id, t3.name, t1.service_id, t2.name AS service_name, t1.attribute, 
		t1.amount, t1.price, t1.total, t1.guarantee, t1.status, IF(t4.count IS NOT NULL, t4.count, 0) AS count_document
		FROM ortb_order_detail AS t1
		LEFT JOIN wstm_page AS t2 ON t1.service_id = t2.id
		LEFT JOIN pdtb_product AS t3 ON t1.product_id = t3.id
		LEFT JOIN (SELECT COUNT(id) AS count, order_detail_id FROM tb_document_customer GROUP BY order_detail_id) AS t4 ON t4.order_detail_id = t1.id
		WHERE t1.order_id = ".$id." ORDER BY t1.maker_date ASC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		for($i = 0; $i < count($list); $i++){

			$sql = "SELECT MAX(t1.checked) AS checked, t1.document_id AS id, t2.name, t1.document_customer_id
				FROM (
					(SELECT document_product_id as document_id, true as checked, id AS document_customer_id FROM tb_document_customer WHERE order_detail_id = ".$list[$i]->order_detail_id.")
					UNION
					(
					SELECT t1.document_id, false as checked, 0 AS document_customer_id 
					FROM tb_document_product AS t1
					LEFT JOIN ortb_order_detail AS t2 ON t2.product_id = t1.product_id
					WHERE t2.id= ". $list[$i]->order_detail_id." AND t2.product_id = ".$list[$i]->id."
					)
				) AS t1
				LEFT JOIN tb_config_product_document AS t2 ON t1.document_id = t2.id
				GROUP BY t1.document_id";

			$query = $this->db->query($sql);

			$list[$i]->document_customer = $query->result_object();
		}

		$data = array('info'=> $info,'list'=>$list);

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function getListServices(){

		$sql = "SELECT t1.id, t1.name

		FROM wstm_page AS t1

		LEFT JOIN wstm_page AS t2 ON t1.parent_id = t2.id

		WHERE t1.status = 1 AND t2.type = 6";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

	public function getListCustomers(){

		$sql = "SELECT id, name, email, phone, address FROM cstb_customer WHERE status = 1 ORDER BY maker_date DESC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success') , $data);
	}

    public function getDocumentProduct(){

		$id = $this->params['id'] ? $this->params['id'] : 0;

		$sql = "SELECT document_id FROM tb_document_product WHERE product_id = ". $id;

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	public function process () {

		$order_id = $this->params['id'] ? $this->params['id'] : 0;

		$data = $this->getdata();

		if($data){

			$info = $data['info'];

			$info['maker_id'] = $this->session->userdata('user_id');

			$info['maker_date'] = date('Y-m-d H:i:s');

			if ($order_id == 0) {
				$info['code'] = time();
				$is = $this->db->insert($this->table, $info);
				$order_id = $this->db->insert_id();
			} else {
				$this->db->where('id', $order_id);
				$is = $this->db->update($this->table, $info);
			}

			if($is == true){

				$list = $data['list']['add'];

				for($i = 0; $i < count($list); $i++){

					$order_detail_id = $list[$i]['id'];

					$document_customer = $list[$i]['document_customer'];

					unset($list[$i]['document_customer']);

					$list[$i]['order_id'] = $order_id;

					$list[$i]['maker_id'] = $this->session->userdata('user_id');

					if ($order_detail_id == 0) {

						$list[$i]['maker_date'] = date('Y-m-d H:i:s');

						$is = $this->db->insert('ortb_order_detail', $list[$i]);

						$order_detail_id = $this->db->insert_id();

					} else {

						$this->db->where('id', $list[$i]['id']);

						$is = $this->db->update('ortb_order_detail', $list[$i]);
					}

					if($is == true){

						for($j = 0; $j < count($document_customer); $j ++){

							$item['order_detail_id'] = $order_detail_id;

							$item['document_product_id'] = $document_customer[$j]['id'];

							$item['status'] = 1;

							if($document_customer[$j]['document_customer_id'] == 0){

								$item['create_user'] = $this->session->userdata('user_id');

								$item['create_date'] = date('Y-m-d H:i:s');

								$this->db->insert('tb_document_customer', $item);

							} else {

								$item['maker_id'] = $this->session->userdata('user_id');

								$item['maker_date'] = date('Y-m-d H:i:s');

								$this->db->where('id', $document_customer[$j]['document_customer_id']);

								$this->db->update('tb_document_customer', $item);
							}
						}
					}
				}

				$remove = $data['list']['remove'];

				for($i = 0; $i < count($remove); $i++){

					$this->db->where('id', $remove[$i]['order_detail_id']);

					$is = $this->db->delete('ortb_order_detail');

					if($is == true){

						$this->db->where_in('id', $remove[$i]['document_customer']);

						$is = $this->db->delete('tb_document_customer');
					}
				}
			}

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			$is == true ? $this->responsesuccess($message, $order_id) : $this->responsefailure($message);
		}
	}
	
	public function processDocumentProduct () {

		$order_detail_id = $this->params['order_detail_id'] ? $this->params['order_detail_id'] : 0;

		$data = $this->getdata();

		if($data){

			$add = $data['add'];

			for($i = 0; $i < count($add); $i ++){

				$item['order_detail_id'] = $order_detail_id;

				$item['document_product_id'] = $add[$i]['id'];

				$item['status'] = 1;

				if($add[$i]['document_customer_id'] == 0){

				$item['create_user'] = $this->session->userdata('user_id');

				$item['create_date'] = date('Y-m-d H:i:s');

				$is = $this->db->insert('tb_document_customer', $item);

				} else {

				$item['maker_id'] = $this->session->userdata('user_id');

				$item['maker_date'] = date('Y-m-d H:i:s');

				$this->db->where('id', $add[$i]['document_customer_id']);

				$is = $this->db->update('tb_document_customer', $item);
				}
			}

			$remove = $data['remove'];

			for($i = 0; $i < count($remove); $i++){

				$this->db->where('id', $remove[$i]['document_customer_id']);

				$is = $this->db->delete('tb_document_customer');

			}

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			$is == true ? $this->responsesuccess($message, $data) : $this->responsefailure($message);
		}else{
			
			$this->responsefailure($this->lang->line('failure'));
		}
	}

	public function remove(){

		$id = $this->params['id'] ? $this->params['id'] : 0;

		$is = false;

		$sql = "SELECT id FROM ortb_order_detail WHERE order_id = ". $id;

		$query = $this->db->query($sql);

		$data = $query->result_object();

		if(count($data) > 0){

			$data = array_keys(array_reduce($data, function ($n, $o) {
				$n[$o->id] = $o;
				return $n;
			}));

			$this->db->where_in('order_detail_id', $data);

			$is = $this->db->delete('tb_document_customer');

			if($is == true){

				$this->db->where('order_id', $id);

				$is = $this->db->delete('ortb_order_detail');

				if($is == true){

					$this->db->where('id', $id);

					$is = $this->db->delete('ortb_order');

					$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

					$is == true ? $this->responsesuccess($message) : $this->responsefailure($message);
				}
			}

		} else {

			$this->db->where('id', $id);

			$is = $this->db->delete('ortb_order');

			$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			$is == true ? $this->responsesuccess($message) : $this->responsefailure($message);
		}
	}
}