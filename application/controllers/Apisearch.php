<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Apisearch extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}
	public function search(){

		$option = isset($this->params['keywords']) ? $this->params['keywords']:'';

		$option = urldecode($option);
		
		$url = base_url() . 'public/products/';
		
		$url2 = base_url() . 'public/contents/';
		
		$result = (object) array( "products" => "", "contents" => "" );
		
		$sqlNormal = "SELECT t1.name, t1.link, t1.maker_date, (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		
		t1.price, t1.price_sale, t1.percent, t1.views, t2.link as parent_link
		
		FROM pdtb_product AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		WHERE t1.status = 1 AND t2.type = 3 AND ( t1.name like '%".$option."%' OR t1.description like '%".$option."%' OR t1.keywords like
		
		'%".$option."%') ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
    	
        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
      
      	$sqlDetail = "SELECT t1.name, t1.link, (CASE WHEN t1.images !='' THEN CONCAT('" . $url . "', t1.images) ELSE '' END) as images,
		
		t1.price, t1.price_sale, t1.percent, t2.link as parent_link
		
		FROM pdtb_product AS t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
		WHERE t1.status = 1 AND t2.type = 3 
		
		AND ( ".$this->processKeySearch('t1.name',$option)." OR ".$this->processKeySearch('t1.description',$option)." 
		
		OR ".$this->processKeySearch('t1.keywords',$option)." ) 
        
        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
    	
        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
	
		$query = $this->db->query($sqlNormal);

		$result->products = $query->result_object();
		
		if(count($result->products) <= 0){
		    
        	$query = $this->db->query($sqlDetail);
        	
          	$list = $query->result_object();
        }
        
        
		$sqlNormal = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , t1.page_id ,  
		
		(CASE WHEN t1.images!='' THEN CONCAT('" . $url2 . "', t1.images) ELSE '' END) AS images ,
			
		t2.name as parent_name,  CONCAT(t3.link, '/', t2.link) AS parent_link
		
		FROM wstm_content as t1 
		
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
		
	    LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id	
		
		WHERE  t1.status=1 AND ( t1.name like '%".$option."%' OR t1.description like '%".$option."%' OR t1.keywords like '%".$option."%') 
        
        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
    	
        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
        
        $sqlDetail = "SELECT t1.id, t1.name, t1.views, t1.description, t1.link,  t1.maker_date , t1.page_id ,  
		
		(CASE WHEN t1.images!='' THEN CONCAT('" . $url2 . "', t1.images) ELSE '' END) AS images ,
			
		t2.name as parent_name,  CONCAT(t3.link, '/', t2.link) AS parent_link
			
		FROM wstm_content as t1 
			
		LEFT JOIN wstm_page AS t2 ON t1.page_id = t2.id
			
		LEFT JOIN wstm_page AS t3 ON t2.parent_id = t3.id		
		
		WHERE  t1.status=1 AND ( ".$this->processKeySearch('t1.name',$option)." OR ".$this->processKeySearch('t1.description',$option)." OR ".$this->processKeySearch('t1.keywords',$option)." ) 
        
        ORDER BY (CASE WHEN t1.name LIKE '%".$option."%' THEN 1 
    	
        WHEN t1.description LIKE '%".$option."%'  THEN 2 ELSE 3 END) ";
	
		$query = $this->db->query($sqlNormal);

		$result->contents = $query->result_object();
      
      	if(count($result->contents) <= 0){
      	    
        	$query = $this->db->query($sqlDetail);
        	
          	$list = $query->result_object();
        }
      
        $this->responsesuccess($this->lang->line('success'), $result);
	}
	
	
	function processKeySearch($field,$keywords)
	{
      
		$result = $field.' like '."'%".$keywords."%'";
		  
		$listKeywords = explode(' ',$keywords);
      
		for($i=0 ; $i < count($listKeywords); $i++){

			$result .= " OR ".$field.' like ';

			$result .= " '%".$listKeywords[$i]."%' ";

		}

		return $result;
      
  	}

    
}