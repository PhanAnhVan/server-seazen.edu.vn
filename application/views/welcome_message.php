<!doctype html>
<html lang="vi">
<!-- <html lang="vi" translate="no"> -->

<head>
    <meta charset="utf-8">
    <meta name="google" content="notranslate">
    <base href="/">
    <meta http-equiv="Cache-control" content="public">
    <meta name="theme-color" content="#fff">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="U8b4Cy5xQncgfY5w-RmieIQiR4pCf2yHxWmCqBBL5bM" />
    <meta name="dmca-site-verification" content="NllkdlJFcEJjOW9XNy9MOGd5MWNBZz090" />
    <meta name="google-site-verification" content="wtyuIltX4xbxDvmymtNUTkSgT6hJr7KnmGoygO8bC5k" />
    <?php echo $seo; ?>
    <script defer>window['globals'] = window['globals'] ? window['globals'] : {}</script>

    <!-- <script>
            (function () {
                var script = document.createElement('script');
                script.src = 'https://unpkg.com/web-vitals/dist/web-vitals.iife.js';
                script.onload = function () {
                    // When loading `web-vitals` using a classic script, all the public
                    // methods can be found on the `webVitals` global namespace.
                    webVitals.getCLS(console.log);
                    webVitals.getFID(console.log);
                    webVitals.getLCP(console.log);
                }
                document.head.appendChild(script);
            }())

        new PerformanceObserver((entryList) => {
            for (const entry of entryList.getEntries()) {
                console.log('LCP candidate:', entry.startTime, entry);
            }
        }).observe({ type: 'largest-contentful-paint', buffered: true });
    </script> -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-NYPG7SZ2TE"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'G-NYPG7SZ2TE');
    </script> -->

    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'"
        href="./assets/fontawesome/all.min.css">

    <!-- ngx DatePicker CSS -->
    <?php echo strpos($_SERVER['REQUEST_URI'],'admin') ? '<link rel="stylesheet" href="https://unpkg.com/ngx-bootstrap@6.1.0/datepicker/bs-datepicker.css">' : '' ?>

    <!-- FancyBox CSS -->
    <link rel="stylesheet" href="./assets/fancybox/jquery.fancybox.min.css">

    <link rel="preload" as="style" onload="this.onload=null;this.rel='stylesheet'"
        href="styles.7db7e2e25838e1bd6640.css" />
</head>

<body>
    <section class="bg-white d-none" id="google_translate">
        <div class="container-main">
            <div class="container-fluid">
                <div class="d-flex justify-content-end justify-content-lg-between align-items-center">
                    <div id="header_info" class="d-none d-lg-block"></div>

                    <div class="btn-group" id="language-group">
                        <!-- <button class="btn bg-transparent btn-sm dropdown-toggle d-inline-flex align-items-center"
                            type="button" data-toggle="dropdown" aria-expanded="false" style="font-size: 1.3rem;">
                            <img src="" alt="" />
                        </button> -->
                        <!-- <div class="dropdown-menu" id="languages"></div> -->
                    </div>
                </div>

                <hr class="my-0" />

                <style type="text/css">
                    #goog-gt-tt {
                        display: none !important;
                    }

                    .goog-te-banner-frame {
                        display: none !important;
                    }

                    .goog-te-menu-value:hover {
                        text-decoration: none !important;
                    }

                    body {
                        top: 0 !important;
                    }

                    #google_translate_element2 {
                        display: none !important;
                    }

                    #languages>a:active {
                        background-color: #e9ecef;
                    }
                </style>

                <div id="google_translate_element2"></div>
                <script type="text/javascript">
                    const path = location.pathname.includes('admin')

                    if (!path) {
                        document.getElementById('google_translate').classList.toggle('d-none')

                        function googleTranslateElementInit2() {
                            new google.translate.TranslateElement({ pageLanguage: 'vi', autoDisplay: false }, 'google_translate_element2');
                        }
                    }
                </script> <!-- TODO: initial function Google Translate -->
                <script type="text/javascript"
                    src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>

                <script type="text/javascript">
                    /* <![CDATA[ */
                    eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] }]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p }('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}', 43, 43, '||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'), 0, {}))
                    /* ]]> */
                </script>

                <script type="text/javascript">
                    'use strict';

                    const adminPage = location.pathname.includes('admin')
                    if (!adminPage) {
                        const TOKEN = {
                            LANGUAGE: 'api/home/getLanguages',
                            INFO: 'api/home/getHeaderInfo',
                        }
                        const URL_LANGUAGE = `${location.origin}/${TOKEN.LANGUAGE}`
                        const URL_INFO = `${location.origin}/${TOKEN.INFO}`

                        const fetchAPI = async (url) => {
                            try {
                                const response = await fetch(url)
                                const data = await response.json()
                                return data
                            } catch (error) {
                                console.log(error)
                            }
                        }

                        const headerInfo = document.getElementById('header_info')
                        fetchAPI(URL_INFO)
                            .then(data => {
                                const info = data.data

                                const html = info?.map(item => {
                                    const { title, value } = item
                                    return `
                                        <a href="${title === 'Phone' ? 'tel:' : title === 'Email' ? 'mailto:' : ''}${value}" class="text-decoration-none text-dark mr-4" style="font-size: 13px;">
                                            ${title}:
                                            <span>${value}</span>
                                        </a>
                                    `
                                })

                                headerInfo.innerHTML = html.join('')
                            })
                            .catch(error => console.log(error))

                        const langGroup = document.getElementById('language-group')
                        fetchAPI(URL_LANGUAGE)
                            .then(data => {
                                const lang = data.data
                                const langActive = JSON.parse(localStorage.getItem('@LANGUAGE/active')) ?? { code: lang.code, name: lang.name, icon: lang.icon }

                                const html = `
                                    <button class="btn bg-transparent btn-sm dropdown-toggle d-inline-flex align-items-center" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 1.3rem;">
                                        <img src="${langActive.icon}" alt="${langActive.name}" height="12" width="16" id="language-group-img" />
                                        <span class="ml-2" id="language-group-label">${langActive.name}</span>
                                    </button>

                                    <div class="dropdown-menu" id="languages">
                                        ${lang.list?.map(item => {
                                            const { name, code, icon } = item

                                            return `
                                                <a class="gflag nturl dropdown-item d-inline-flex align-items-center text-capitalize text-decoration-none text-dark" href="javascript:" onclick="handleChangeLang('${code}', '${name}', '${icon}', event)" style="font-size: 14px;">
                                                    <img src="${icon}" height="12" width="16" alt="${name}" />
                                                    <span class="ml-2">${name}</span>
                                                </a>
                                            `
                                        }).join('')}
                                    </div>
                                `

                                langGroup.innerHTML = html
                            })
                            .catch(error => console.log(error))
                    }
                </script>

                <script type="text/javascript">
                    // TODO: handle event change language
                    let languageActive = {}

                    const handleChangeLang = (code, name, icon, event) => {
                        const img = document.getElementById('language-group-img')
                        const label = document.getElementById('language-group-label')

                        if (languageActive.code === code) return

                        doGTranslate(`vi|${code}`); // TODO: Change language

                        img.src = icon
                        label.innerHTML = name

                        languageActive.code = code // TODO: Set language active
                        languageActive.icon = icon
                        languageActive.name = name

                        localStorage.setItem('@LANGUAGE/active', JSON.stringify(languageActive))

                        return false;
                    }
                </script>
            </div>
        </div>
    </section>
    
    <app-root></app-root>

    <!-- Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns"
        crossorigin="anonymous"></script>

    <!-- FancyBox JS -->
    <script async src="./assets/fancybox/jquery.fancybox.min.js"></script>

    <script src="./assets/js/js.js"></script>
    
    <script src="runtime.70fee40ac95735ded681.js" defer></script>
    <script src="polyfills-es5.4adcc2904700c1c1eb0a.js" nomodule defer></script>
    <script src="polyfills.4d86510d37a9f0331f27.js" defer></script>
    <script src="main.5e896be0dddf477a9afc.js" defer></script>
    
    <!-- Ckeditor -->
    <?php echo strpos($_SERVER['REQUEST_URI'],'admin') ? '<script src="//cdn.ckeditor.com/4.16.0/full-all/ckeditor.js"></script>' : ''; ?>
</body>

</html>