<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Title extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "wstm_title";
		
		$this->history = "./public/history/website/";

		$this->website = "./public/website/";

		if (!is_dir($this->history)) {

			@mkdir($this->history, 0757);
		}
		if (!is_dir($this->website)) {

			@mkdir($this->slide, 0757);
		}
	}

	public function getrowcompany()
	{

		$sql = "SELECT id, name, logo, logo_white, shortcut, description, keywords FROM wstm_title where id = 1";

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$list = ($list != null) ? $list :  array();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
		
	}
	public function company()
	{

		$data = $this->getdata();

		$is = false;

		if ($data !== null) {

			if (array_key_exists('shortcut', $data)) {
				
				$data['shortcut'] = $this->processimages($data['shortcut']);
			} else {

				$data['shortcut'] = '';
			}

			if (array_key_exists('logo', $data)) {

				$data['logo'] = $this->processimages($data['logo']);
			} else {

				$data['logo'] = '';
			}

			if (array_key_exists('logo_white', $data)) {

				$data['logo_white'] = $this->processimages($data['logo_white']);
			} else {

				$data['logo_white'] = '';
			}

			$data['maker_id'] = $this->session->userdata('user_id');;

			$data['maker_date'] = date('Y-m-d H:i:s');

			$this->db->where('id', 1);
			
			$is = $this->db->update($this->table, $data);
			
		}

		if ($is == true) {

			$message = $this->lang->line('success');

			$this->responsesuccess($message);
		} else {

			$message = $this->lang->line('failure');

			$this->responsefailure($message);
		}
	}
}
