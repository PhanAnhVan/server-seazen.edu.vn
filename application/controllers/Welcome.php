<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	public function index()
	{
		$sql ="SELECT * FROM wstm_title WHERE id=1";
		
		$query = $this->db->query($sql);
		
		$title = $query->row_object();
		
		$link = @explode(".html",end(explode("/",ltrim(explode("?",urldecode(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)))[0],"/"))))[0];
		
		$filed = "name , description , keywords, detail";
		
		$base = base_url()."public/pages/";
		
		$sql ="select detail, ".$filed.",  IF(images != '', CONCAT('".$base."',images), '') as images , if( title != '', title, name ) AS name from wstm_page where link='".$link."'";
		
		$query = $this->db->query($sql);
		
		$row = $query->row_object();
		
		if(empty($row)){
			
			$base = base_url()."public/contents/";
			
			$sql ="select  ".$filed." ,   CONCAT('".$base."',images) as images from wstm_content where link='".$link."'";
						
			$query = $this->db->query($sql);
			
			$row = $query->row_object();
          
          	if(empty($row) || !$row){
			
                $base = base_url()."public/products/";

                $sql ="select  ".$filed." ,   CONCAT('".$base."',images) as images from pdtb_product where link='".$link."'";

                $query = $this->db->query($sql);

                $row = $query->row_object();
          	}
		}
		
		$optionseo = $this->seooption($title , (empty($row)) ? (object) array() : $row);	
		
		$data = array('seo' => $this->seohtml($optionseo), 'html' => isset($row->detail) ? $row->detail :"" );
		
		$this->load->view('welcome_message',$data);
		
	}
	
	public function seooption($title ,$row){
		
		$base = $this->config->item('base_url');
		
		$name = property_exists($row,'name') ? $row->name : $title->name;
      
      	$detail = property_exists($row,'detail') ? trim(substr(html_entity_decode(strip_tags($row->detail)),0,200), " \t.") : '';
      
    	$detail = substr($detail,0,strripos($detail, ' ')); 
		
		$description = property_exists($row,'description') ? ($row->description != '' ? $row->description : $detail) : $title->description;
		
		$keywords = property_exists($row,'keywords') ? $row->keywords : $title->keywords;
		
		try{
			
			@$keywords = implode(", ", json_decode($keywords));
			
			@$keywords .= implode(", ", json_decode($title->keywords));
			
			
		}catch(Exception $e) {
			
			@$keywords = implode(", ", json_decode($keywords));
			
			@$keywords .= implode(", ", json_decode($title->keywords));
		}
		
		$shortcut = $base.'public/website/'.$title->shortcut;
		
		$logo = $base.'public/website/'.$title->logo;
		
		$images = property_exists($row,'images') ? $row->images : '';
	
		return array(
			'title'			=>		$name,
			
			'logo'          =>      $logo,
			
			'shortcut'		=>		$shortcut,
			
			'name'			=>		$name,
			
			'description'	=>		trim($description),
			
			'keywords'		=>		$keywords,
			
			'tags'			=>		$keywords,
			
			'language'		=>		'vi',
			
			'copyright'		=>		'Copyright © 2021 by Chips',
				
			'og_url'		=>		'https://seazen.edu.vn'.$_SERVER['REQUEST_URI'],
			
			'og_title'		=>		$name,
			
			'og_description'=>		$description,
			
			'og_image'		=>		$images,
			
			'og_full_image'	=>		$images
			);
	}

	public function seohtml($data){

		$title = (isset($data['title'])) ? $data['title'] : '';

		$description = (isset($data['description']) && $data['description'] !='') ? $data['description'] : '' ;

		$keywords = (isset($data['keywords'])) ? $data['keywords'] : '';

		$shortcut =(isset($data['shortcut'])) ? $data['shortcut'] : '';
		
		$logo =(isset($data['logo'])) ? $data['logo'] : '';
		
		$name = (isset($data['name'])) ? $data['name'] : '';
		
		$tags  = (isset($data['tags'])) ? $data['tags'] : '';
		
		$copyright = (isset($data['copyright'])) ? $data['copyright'] : 'Copyright 2009 - 2021';
		
		$og_url = (isset($data['og_url'])) ? $data['og_url'] : '';
		
		$og_title = (isset($data['og_title'])) ? $data['og_title'] : '';
		
		$og_description = (isset($data['og_description'])) ? $description : '';
		
		$og_image = (isset($data['og_image']) &&  $data['og_image'] !='' ) ? $data['og_image'] :  $shortcut;
		
		$og_full_image = (isset($data['og_full_image']) &&  $data['og_image'] !='' ) ? $data['og_full_image'] :  $logo;

		$str  =" <title>".$name." </title>";
		
		$str .=" <meta name='description' content='".$description."' />";

		$str .=" <meta name='robots' content='index,follow' />";

      	$str .=" <meta name='author' content='Công ty Cổ phần Chips' />";

      	$str .=" <meta name='copyright' content='".$copyright."' />";

      	$str .=" <meta name='RATING' content='GENERAL' />";

      	$str .=" <meta name='GENERATOR' content='".$title."' />";

      	$str .=" <meta property='og:type' content='website' />";

      	$str .=" <meta property='og:site_name' content='".$title."' />";

		$str .=" <meta property='og:title' content='".$og_title."'/>";

		$str .=" <meta property='og:description' content='".$og_description."' />";

      	$str .=" <meta property='og:url' content='".$og_url."' />";

		$str .=" <meta property='og:image' content='".$og_image."' />";

      	$str .=" <meta property='og:locale' content='vi_VN' />";

		$str .=" <meta property='article:tag' content='".$tags."' />";

		$str .=" <meta property='article:author' content='".$title."' />";

		$str .=" <meta content='".$tags."' />";

    	$str .=" <meta name='distribution' content='Global' />";

    	$str .=" <meta http-equiv='Content-Language' content='vi' />";

    	$str .=" <meta name='Language' content='vi' />";

    	$str .=" <meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1' />";

		$str .=" <meta name='resource-type' content='Document' />";

    	$str .=" <meta name='revisit-after' content='1 days' />";

      	$str .=" <link rel='shortcut icon' href='".$shortcut."' type='image/x-icon' />";

      	$str .=" <link rel='apple-touch-icon' href='".$shortcut."' />";

      	$str .=" <link rel='apple-touch-icon-precomposed' href='".$shortcut."' />";

    	$str .=" <meta http-equiv='x-dns-prefetch-control' content='on' />";

      	$str .=" <link rel='alternate' hreflang='vi-vn' href='".$og_url."' />";

      	$str .=" <meta name='keywords' content='".$keywords."' />";

      	$str .=" <meta property='fb:app_id' content='' />"; 

		$str .=" <meta property='og:image:alt' content='".$og_title."' />";

      	$str .=" <meta property='og:full_image' content='".$og_full_image."' />";

      	$str .=" <meta name='twitter:card' content='summary' />";
		
		$str .=" <meta name='twitter:title' content='".$og_title."' />";
		
		$str .=" <meta name='twitter:description' content='".$og_description."' />";
		
		$str .=" <meta name='twitter:site' content='".$og_url."' />";
		
		$str .=" <meta name='twitter:image' content='".$og_image."' />";
		
		$str .=" <meta name='twitter:image:src' content='".$og_image."' />";

      	$str .=" <link rel='canonical' href='".$og_url."' />";


      	$str .=" <meta name='medium' content='image' />";

      	$str .=" <link rel='image_src' href='".$og_image."' />";

      	$str .=" <meta name='audience' content='all' />";


		return $str;
	}
}