<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Partner extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "tb_partner";

		$this->history = "./public/history/partner/";

		$this->avatar = "./public/partner/";

		if (!is_dir($this->history)) {

			mkdir($this->history, 0757);
		}
		if (!is_dir($this->avatar)) {

			mkdir($this->avatar, 0757);
		}
	}

	public function getlist()
	{
		$sql = "SELECT * FROM " . $this->table . " ORDER BY maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT *  FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);
				
			if (array_key_exists('logo', $data)) {

				$data['logo'] = $this->processimages($data['logo']);	
				
			}else{

				$data['logo'] = '';
			}
			
			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$company = array_key_exists('company', $data) ?  $data['company'] : '';

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE company = '" . $company . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
				
			} else {

				$message = $this->lang->line('checkExitName');
			}

		} else {

			$message = $this->lang->line('failure');

		}
		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$this->db->where('id', $id);

			$is = $this->db->delete($this->table);

			$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

		} else {
			
			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
