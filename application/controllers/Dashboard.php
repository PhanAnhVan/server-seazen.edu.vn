<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends MY_Controller{
	
	public function getdashboard(){
		
		$table = array(
          	'wstm_page',
			'pdtb_product',
          	'tb_class',
          	'tb_student',
			'wstm_content',
          	'tb_document',
			'wstm_slide',
		);
		$title=array(	
          	'Ngành học',
			'Khóa học',
          	'Lớp học',
          	'Học viên',
			'Tin tức',
			'Tài liệu',
			'Banner'
		);
		$class = array(
			'fab fa-product-hunt',
			'fa fa-id-card',
          	'fas fa-graduation-cap',
          	'fa fa-users',
			'fas fa-bookmark',
          	'fad fa-file-alt',
			'fas fa-photo-video'
		);
      
		$data=array();
		for ($i=0; $i < count($table); $i++) { 
			$sqlon ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='1'";
            $sqloff ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='0'";
			if($table[$i] == 'wstm_page'){
				$sqlon .= ' AND type = 3 AND parent_id != 0';
				$sqloff .= ' AND type = 3 AND parent_id != 0';
			}
          	if($table[$i] == 'tb_class'){
				$sqlon ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='1'";
            	$sqloff ="SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status='2'";
			}
          	
			$on = $this->db->query($sqlon)->row_object();
            $off = $this->db->query($sqloff)->row_object();
			//'color'=>$color[$i],
			array_push($data,array('class'=> $class[$i],'title'=>$title[$i] ,'on' => $on->count ,'off' => $off->count));
		}
		$this->responsesuccess($this->lang->line('success'), $data);
	}
	
	public function getUnRead(){
		$table = array(
			'wstb_contact',
			'ortb_order',
		);
		$key=array(			
			'contact',
			'cart',
		);
		$data=array();
		for ($i=0; $i < count($table); $i++) { 
			$sql = '';
			if($key[$i] == 'contact'){
				$sql = "SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE checked IS NULL OR checked = 0";
			}else{
				$sql = "SELECT COUNT(id) AS count FROM ".$table[$i]." WHERE status = 1";
			}
			$result = $this->db->query($sql)->row_object();
			array_push($data,array('key'=> $key[$i],'amount' => $result->count));
		}
		
		$this->responsesuccess($this->lang->line('success'), $data);
	}
}