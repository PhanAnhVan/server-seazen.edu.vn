<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Attribute extends MY_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->table = "pdtm_attribute";
	}
	public function getlist(){
		
		$sql="SELECT id, name, maker_date, status FROM ".$this->table . " WHERE parent_id = 0" ;
		
		$query = $this->db->query($sql);
		
		$list = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $list);
		
	}
	public function getlistall(){
		
		$sql="SELECT id, name, maker_date, parent_id, status FROM ".$this->table ;
		
		$query = $this->db->query($sql);
		
		$list = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $list);
		
	}
	public function getrow(){
		
		$id = $this->params['id'];
		
		$sql="SELECT * FROM ".$this->table." where id=".$id;
		
		$query = $this->db->query($sql);
		
		$list = $query->row_object();

		$sql="SELECT * FROM ".$this->table." where parent_id=".$id;

		$query = $this->db->query($sql);
		
		$list->parent = $query->result_object();
		
		$this->responsesuccess($this->lang->line('success') , $list);
	}
	
	public function process(){
		
		$data = $this->getdata();
		
		$is = false;
		
        $id = (isset($id)) ? $id : (array_key_exists('id',$data) ? (int) $data['id'] : 0);
		
		if ($data !== null) {

			$id = isset($id) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE name='" . $data['name'] . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = (array_key_exists('parent_id', $data) &&$data['parent_id'] > 0) ? $this->lang->line('checkExitrValueAttribute') : $this->lang->line('checkExitrAttribute');
			}
		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);

		} else {

			$this->responsefailure($message);
		}
		
	}
	
	public function remove(){
		
		$id = $this->params['id'];

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM pdtm_attribute where parent_id =".$id;

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);
			} else {

				$message .= 'Đã tồn tại giá trị';
			}
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	public function removeValueAtribute(){
		
		$id = $this->params['id'];

		$is = false;

		$message = $this->lang->line('failure');

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM pdtb_product_price_att where att_id_value like '%".$id."%' ";

			if ($this->db->query($sql)->row_object()->count == 0) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);
			} else {

				$message .= 'Giá trị đã được cấu hình trong giá sản phẩm.';
			}
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}