<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Languages extends MY_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->table = "tb_language";

		// $this->history = './public/history/settings/';

		$this->languages = './public/language/';

		// if (!is_dir($this->history)) {

		// 	mkdir($this->history, 0757);
		// }
		if (!is_dir($this->languages)) {

			mkdir($this->languages, 0757);
		}
	}
	
	public function getList()
	{
		$sql = "SELECT * from ". $this->table;

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess(null, $list);
	}

	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);
				
			if (array_key_exists('icon', $data)) {

				$data['icon'] = $this->processimages($data['icon']);	
				
			}else{

				$data['icon'] = '';
			}
			
			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$code = array_key_exists('code', $data) ?  $data['code'] : '';

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE code = '" . $code . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
				
			} else {

				$message = $this->lang->line('checkExitCode');
			}

		} else {

			$message = $this->lang->line('failure');

		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT *  FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function changeStatus() {
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$status = isset($this->params['status']) ? $this->params['status'] : 0;
		
		$is = false;
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('status' => $status));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
	}

	// public function changePin(){
		
	// 	$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
	// 	$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
	// 	$this->db->where('id', $id);

	// 	$is = $this->db->update($this->table, array('pin' => $pin, 'maker_date' => date('Y-m-d H:i:s')));
		
	// 	$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

	// 	($is == true) ? $this->responsesuccess($message) : $this->responsefailure($message);
		
	// }

	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE id=" . $id;

			if ($this->db->query($sql)->row_object()->count == 1) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
			} else {

				$message = $this->lang->line('isDelete');
			}
		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
}
