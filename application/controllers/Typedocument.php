<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Typedocument extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "tb_type_document";
		
	}
	
	public function getlist()
	{
    
		$sql = "SELECT t1.id, t1.code, t1.name,   t1.status , t1.maker_date
		
		FROM ". $this->table ." AS t1 
		
		ORDER BY t1.maker_date DESC ";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$message = $this->lang->line('success');

		$this->responsesuccess($message, $list);
	}
	
	public function getrow()
	{

		$id = $this->params['id'];

		$sql = "SELECT * FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	
	public function process()
	{

		$data = $this->getdata();

		$id = isset($this->params['id']) && $this->params['id'] > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);
			
			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$name = array_key_exists('name', $data) ?  $data['name'] : '';

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE name='" . $name . "'";


			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$is = $this->db->insert($this->table, $data);

				} else {

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);
					
				}

				$message = $is == true ?  $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = $this->lang->line('checkExitnameSlide');
			}
		}
		if ($is == true) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{

		$id = $this->params['id'];

		$is = false;
		    
	    $this->db->where('id', $id);

	    $is = $this->db->delete($this->table);
	   
	    if ($is == true) {

    		$message = $this->lang->line('success');
    
    		$this->responsesuccess($message);
		
		} else {

			$message = $this->lang->line('failure');

			$this->responsefailure($message);
		}
	
	}
}
