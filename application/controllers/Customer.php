<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "tb_student";

		$this->history = "./public/history/student/";

		$this->avatar = "./public/student/";

		if (!is_dir($this->history)) {

			@mkdir($this->history, 0757);
		}
		if (!is_dir($this->avatar)) {

			@mkdir($this->avatar, 0757);
		}
	}

	public function getlist()
	{

		$url = base_url() . 'public/student/';

		$sql = "SELECT t1.id, t1.avatar, t1.name, t1.code, t1.sex, t1.birth_date, t1.academic_level, t1.object_name, 

				t1.nation, t1.is_poor, t1.is_revolution, t1.code_company, t1.email, t1.phone, t1.address, t1.weight, t1.height, 

				t1.facebook, t1.idno, t1.idno_place, t1.idno_date, t1.is_register_form, t1.is_certification, t2.majors, t2.courses

		 		FROM tb_student AS t1 

				LEFT JOIN (SELECT t2.student_id, t3.name AS majors,

		  			(CASE WHEN t4.name != '' THEN GROUP_CONCAT(t4.name SEPARATOR ', ') ELSE '' END) AS courses 

		  			FROM tb_student_product AS t2 

		  			LEFT JOIN wstm_page AS t3 ON t2.page_id = t3.id 

		  			LEFT JOIN pdtb_product AS t4 ON t2.product_id = t4.id)

	  			AS t2 ON t1.id = t2.student_id 

				ORDER BY t1.maker_date DESC";

		$query = $this->db->query($sql);

		$data = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $data);
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT *  FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}

	public function getStudentLargestCode()
	{
		$sql = "SELECT code FROM ". $this->table ." ORDER BY code DESC LIMIT 1";

		$data = $this->db->query($sql)->row_object();

		if(isset($data->code)) {

			$this->responsesuccess($this->lang->line('success'), $data->code);
			
		} else {
			
			$this->responsefailure($this->lang->line('failure'));

		}
	}

	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('avatar', $data)) {

				$data['avatar'] = $this->processimages($data['avatar']);
			} else {

				$data['avatar'] = '';
			}

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE code ='" . $data['code'] . "'";

			if ($id > 0) {

				$sql .= " AND id != " . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				// $sql = "SELECT count(id) as count FROM " . $this->table . " WHERE code ='" . $data['code'] . "'";

				// if ($id > 0) {

				// 	$sql .= " AND id!=" . $id;
				// }

				// if ($this->db->query($sql)->row_object()->count == 0) {

					if ($id == 0) {

						$data['password'] = array_key_exists('password', $data) ?  sha1($data['password']) : '';

						$is = $this->db->insert($this->table, $data);

					} else {

						$this->db->where('id', $id);

						$is = $this->db->update($this->table, $data);
					}

					$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

				// } else {

				// 	$message = $this->lang->line('CheckEmail');
				// }

			} else {

				$message = $this->lang->line('CheckCode');
			}

		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$data['id'] = $this->db->insert_id();

			$this->responsesuccess($message, $data);

		} else {

			$this->responsefailure($message);
		}
	}
	
	public function changepassword()
	{
		$is = false;

		$data = $this->getdata();

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($data !== null) {

			$this->db->where('id', $id);

			$is = $this->db->update($this->table, array('password' => sha1($data['password'])));

		}

		$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{
		$is = false;

		$id = isset( $this->params['id']) ? $this->params['id'] : 0;

        if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE id=" . $id;

			if ($this->db->query($sql)->row_object()->count > 0 && $this->checkedRemove($id)) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = $this->lang->line('isDeleteStudent');
			}

		} else {

			$message = $this->lang->line('failure');
		}
			
		if ($is) {

			$this->responsesuccess($message);

		} else {

			$this->responsefailure($message);
		}
	}
	
	public function checkedRemove($id) {

		$skip = false;

		$sql = "SELECT count(student_id) as count FROM tb_student_product WHERE student_id = " . $id;

		if ($this->db->query($sql)->row_object()->count == 0) {

			$skip = true;
					
		}

		return $skip;
	}
}
