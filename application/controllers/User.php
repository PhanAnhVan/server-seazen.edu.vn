<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		$this->table = "hrtb_user";

		$this->history = "./public/history/avatar/";

		$this->avatar = "./public/avatar/";

		if (!is_dir($this->history)) {

			mkdir($this->history, 0757);
		}
		if (!is_dir($this->avatar)) {

			mkdir($this->avatar, 0757);
		}
	}

	public function getlist()
	{
		$sql = "SELECT t1.id, t1.code, t1.avatar, t1.parent_id, t1.name,t1.format_work,t1.academic_level, t1.teacher, t1.phone, t1.email, t1.status, t1.maker_date, t1.type,  t2.name as parent_name
		
			FROM " . $this->table . "  AS t1 

			LEFT JOIN " . $this->table . "  AS t2 ON t1.parent_id = t2.id ORDER BY t1.maker_date DESC";

		$query = $this->db->query($sql);

		$list = $query->result_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	
	public function getAcademics() 
	{
		
		$sql = "SELECT id, name FROM ". $this->table ." WHERE type = 2 AND status = 1";
		
		$list = $this->db->query($sql)->result_object();
	
		$this->responsesuccess($this->lang->line('success'), $list);
		
	}

	public function getrow()
	{
		$id = $this->params['id'];

		$sql = "SELECT *  FROM " . $this->table . " WHERE id=" . $id;

		$query = $this->db->query($sql);

		$list = $query->row_object();

		$this->responsesuccess($this->lang->line('success'), $list);
	}
	
	public function process()
	{
		$data = $this->getdata();

		$id = isset($this->params['id']) > 0 ? $this->params['id'] : 0;

		$is = false;

		if ($data !== null) {

			$id = (isset($id)) && $id > 0 ? $id : (array_key_exists('id', $data) ? $data['id'] : 0);

			if (array_key_exists('avatar', $data)) {

				$data['avatar'] = $this->processimages($data['avatar']);
			} else {

				$data['avatar'] = '';
			}

			// $data['code'] = time();

			$data['maker_id'] = $this->session->userdata('user_id');

			$data['maker_date'] = date('Y-m-d H:i:s');

			$email = array_key_exists('email', $data) ?  $data['email'] : '';

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE email='" . $email . "'";

			if ($id > 0) {

				$sql .= " AND id!=" . $id;
			}

			if ($this->db->query($sql)->row_object()->count == 0) {

				if ($id == 0) {

					$data['password'] =  array_key_exists('password', $data) ?  sha1($data['password']) : sha1('admin@123');

					$is = $this->db->insert($this->table, $data);

				} else {

					if (array_key_exists('password', $data)) {

						unset($data['password']);
					}

					$this->db->where('id', $id);

					$is = $this->db->update($this->table, $data);

				}

				$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');
				
			} else {

				$message = $this->lang->line('checkExitEmail');
			}

		} else {

			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function changepassword()
	{
		$is = false;

		$data = $this->getdata();

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($data !== null) {

			$this->db->where('id', $id);

			$is = $this->db->update($this->table, array('password' => sha1($data['password'])));

		}

		$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is) {

			$this->responsesuccess($message, $id);
		} else {

			$this->responsefailure($message);
		}
	}
	
	public function changeToken(){
		
		$id = isset($this->params['id']) ? $this->params['id'] : 0;
		
		$pin = isset($this->params['pin']) ? $this->params['pin'] : 0;
		
		$is = false;
		
		if( $pin == 1){
            $sql = "UPDATE ".$this->table." SET pin = 0 ";
            $is = $this->db->query($sql);
        }
		
		$this->db->where('id', $id);

		$is = $this->db->update($this->table, array('pin' => $pin));
		
		$message = ($is == true) ? $this->lang->line('success') : $this->lang->line('failure');

		if ($is == true) {

			$this->responsesuccess($message);
			
		} else {
			$this->responsefailure($message);
		}
	}
	
	public function remove()
	{
		$is = false;

		$id = $this->params['id'] && $this->params['id'] > 0 ? $this->params['id'] : 0;

		if ($id > 0) {

			$sql = "SELECT count(id) as count FROM " . $this->table . " WHERE (is_delete != 1 OR is_delete IS NULL) AND id=" . $id;

			if ($this->db->query($sql)->row_object()->count > 0 && $this->checkedRemove($id)) {

				$this->db->where('id', $id);

				$is = $this->db->delete($this->table);

				$message = $is == true ? $this->lang->line('success') : $this->lang->line('failure');

			} else {

				$message = $this->lang->line('isDelete');
			}

		} else {
			
			$message = $this->lang->line('failure');
		}

		if ($is) {

			$this->responsesuccess($message);
		} else {

			$this->responsefailure($message);
		}
	}

	public function checkedRemove($id) {

		$skip = false;

		$sql = "SELECT count(teacher_id) as count FROM tb_class_product WHERE teacher_id = " . $id;

		if ($this->db->query($sql)->row_object()->count == 0) {

			$skip = true;
					
		}

		return $skip;
	}
}
